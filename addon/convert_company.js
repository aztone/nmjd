var csv = require('fast-csv');
var fs = require('fs');
var data = [];
csv.fromPath("1.csv")
    .on("data", function(d){
        data.push({com_region:d[0], com_province:d[1], com_city:d[2]});
    })
    .on("end", function(){
        console.log(data);
        fs.writeFile('1.json', JSON.stringify(data));
    });