'use strict';

/* Services */
// Demonstrate how to register services
var app = angular.module('app.services', []);

app.factory('$myhttp', function ($http, $q) {
    $http.defaults.cache = false;
    var service = {};
    service.get = function (url, params, cache) {
        if (!cache) {
            cache = false;
        }
        var defered = $q.defer();
        if (!params) {
            params = {};
        }
        params.timestamp = new Date().getTime();

        $http({method: 'GET', url: url, cache: cache, params: params}).success(function (data) {

            if (data.err) {
                defered.reject(data.err);
                console.log('get(' + url + ',' + JSON.stringify(params) + ')=' + data.err);
            } else {
                defered.resolve(data.result);
            }
        }).error(function () {
            defered.reject('网络异常');
            console.log('get(' + url + ',' + JSON.stringify(params) + ')=网络异常');
        });
        return defered.promise;
    };

    service.post = function (url, params, cache) {
        if (!cache) {
            cache = false;
        }
        var defered = $q.defer();
        $http({method: 'POST', url: url, data: params, cache: cache}).success(function (data) {
            if (data.err) {
                defered.reject(data.err);
                console.log('post(' + url + ',' + JSON.stringify(params) + ')=' + data.err);
            } else {
                defered.resolve(data.result);
            }
        }).error(function () {
            defered.reject('网络异常');
            console.log('post(' + url + ',' + JSON.stringify(params) + ')=网络异常');
        });
        return defered.promise;
    };

    service.put = function (url, params, cache) {
        if (!cache) {
            cache = false;
        }
        var defered = $q.defer();
        $http({method: 'PUT', url: url, data: params, cache: cache}).success(function (data) {
            if (data.err) {
                defered.reject(data.err);
                console.log('put(' + url + ',' + JSON.stringify(params) + ')=' + data.err);
            } else {
                defered.resolve(data.result);
            }
        }).error(function () {
            defered.reject('网络异常');
            console.log('put(' + url + ',' + JSON.stringify(params) + ')=网络异常');
        });
        return defered.promise;
    };

    service.del = function (url, cache) {
        if (!cache) {
            cache = false;
        }
        var defered = $q.defer();
        $http({method: 'DELETE', url: url, cache: cache}).success(function (data) {
            if (data.err) {
                defered.reject(data.err);
                console.log('delete(' + url + ')=' + data.err);
            } else {
                defered.resolve(data.result);
            }
        }).error(function () {
            defered.reject('网络异常');
            console.log('delete(' + url + ')=网络异常');
        });
        return defered.promise;
    };

    service.jsonp = function (url, params) {
        var defered = $q.defer();
        if (!params) {
            params = {callback: 'JSON_CALLBACK'};
        }
        $http({method: 'jsonp', url: url, cache: false, params: params})
            .success(function (data) {
                if (data.err) {
                    defered.reject(data.err);
                } else {
                    defered.resolve(data.result);
                }
            }).error(function () {
                defered.reject('网络异常');
            });
        return defered.promise;
    };
    return service;
});

app.factory('CommonService', function ($rootScope) {
    var service = {};
    service.error = function (msg) {
        $rootScope.$broadcast('message', {error: msg});
    };

    service.info = function (msg) {
        $rootScope.$broadcast('message', {info: msg});
    };

    service.success = function (msg) {
        $rootScope.$broadcast('message', {success: msg});
    };

    return service;
});

app.factory('UserService', function ($myhttp,$q) {
    var service = {};
    service.has = function (id) {
        if (service.user) {
            return _.contains(service.user.features, id);
        } else{
            return $myhttp.get('/access/info').then(function (data) {
                service.user = data;
                return _.contains(service.user.features, id);
            })
        }
    };
    service.getUser = function () {
        var defered = $q.defer();
        if (service.user){
            defered.resolve(service.user);
        }else{
            $myhttp.get('/access/info').then(function (data) {
                service.user = data;
                defered.resolve(service.user);
            })
        }
        return defered.promise;
    };
    return service;
});

app.factory('CommonDataService', function ($q, $myhttp) {
    var service = {
        version: []
    };

    service.getCommonData = function () {
        var serverVer;

        function getCurVer(type) {
            var curVer = _.find(service.version, {type: type});
            //console.log('cur ver', type, curVer ? curVer.version : undefined);
            return curVer ? curVer.version : undefined;
        }

        function getVer(serverVer, type) {
            var ver = _.find(serverVer, {type: type});
            //console.log('ver', type, serverVer, ver ? ver.version : undefined);
            return ver ? ver.version : undefined;
        }

        function setVer(type, version) {
            var curVer = _.find(service.version, {type: type});
            if (version) {
                if (curVer) {
                    curVer.version = version;
                } else {
                    service.version.push({type: type, version: version })
                }
            }
        }

        return $myhttp.get('/api/version').then(function (data) {
            serverVer = data;
            var curVer = getCurVer('metadata');
            var ver = getVer(serverVer, 'metadata');
            if (!curVer || !ver || curVer < ver) {
                return $myhttp.get('/api/metadata/all').then(function (data) {
                    service.metadata = data;
                    setVer('metadata', ver);
                });
            }
        }).then(function () {
            var curVer = getCurVer('terminal');
            var ver = getVer(serverVer, 'terminal');
            if (!curVer || !ver || curVer < ver) {
                return $myhttp.get('/api/terminal').then(function (data) {
                    service.terminals = data;
                    setVer('terminal', ver);
                });
            }
        }).then(function () {
            var curVer = getCurVer('user');
            var ver = getVer(serverVer, 'user');
            if (!curVer || !ver || curVer < ver) {
                return $myhttp.get('/api/user').then(function (data) {
                    service.users = data;
                    setVer('user', ver);
                });
            }
        }).then(function () {
            return {
                metadata: service.metadata,
                terminals: service.terminals,
                users: service.users
            }
        })
    };

    service.getMetadata = function (prefix, tid) {
        var deferred = $q.defer(), metadata = service.metadata;
        if (service.metadata) {
            deferred.resolve(service.metadata);
        } else {
            prefix = prefix || 'api';
            $myhttp.get('/' + prefix + '/metadata/all', {tid: tid}).then(function (result) {
                service.metadata = _.filter(result, {state: 'Y'});
                deferred.resolve(service.metadata);
            }, function (e) {
                deferred.reject(e);
            });
        }
        return deferred.promise;
    };

    service.brandTypeChange = function (brand_id, type_id, prefix, tid) {
        if (service.metadata) {
            return service.getBrandTypeChange(brand_id, type_id, prefix, tid)
        } else {
            return service.getMetadata(prefix, tid).then(function () {
                return service.getBrandTypeChange(brand_id, type_id, prefix, tid);
            });
        }
    };
    service.getBrandTypeChange = function (brand_id, type_id, prefix, tid) {
        prefix = prefix || 'api';
        var deferred = $q.defer(), metadata = service.metadata;
        var rtn = {brand_id: brand_id, type_id: type_id};
        if (type_id && brand_id) {
            rtn.brandList = _.filter(metadata, function (e) {
                return _.contains([brand_id], e.key);
            });
            rtn.typeList = _.filter(metadata, function (e) {
                return _.contains([type_id], e.key);
            });
            deferred.resolve(rtn);
        }
        if (!brand_id && !type_id) {
            rtn.brandList = _.filter(metadata, {context: 'product.brand', state: 'Y'});
            rtn.typeList = _.filter(metadata, {context: 'product.category', state: 'Y'});
            rtn.brand_id = undefined;
            rtn.type_id = undefined;
            deferred.resolve(rtn);
        }

        if (brand_id && !type_id) {
            $myhttp.get('/' + prefix + '/brand_category/brand/' + brand_id, {tid: tid}).then(function (data) {
                var typeIds = _.pluck(data, 'category_id');
                if (!_.contains(typeIds, type_id)) {
                    rtn.type_id = undefined;
                }
                rtn.brandList = _.filter(metadata, {context: 'product.brand', state: 'Y'});
                rtn.typeList = _.filter(metadata, function (e) {
                    if (e.state == 'Y') {
                        return _.contains(typeIds, e.key);
                    }
                });
                deferred.resolve(rtn);
            }, function (e) {
                deferred.reject(e);
            })
        }
        if (type_id && !brand_id) {
            $myhttp.get('/' + prefix + '/brand_category/category/' + type_id, {tid: tid}).then(function (data) {
                var brandIds = _.pluck(data, 'brand_id');
                if (!_.contains(brandIds, brand_id)) {
                    rtn.brand_id = undefined;
                }
                rtn.typeList = _.filter(metadata, {context: 'product.category', state: 'Y'});
                rtn.brandList = _.filter(metadata, function (e) {
                    if (e.state == 'Y') {
                        return _.contains(brandIds, e.key);
                    }
                });
                deferred.resolve(rtn);
            }, function (e) {
                deferred.reject(e);
            })
        }
        return deferred.promise;
    };

    return service;
});

app.factory('$wcjssdk',function($myhttp, $q) {
    var service = {};
    var appid;
    var timestamp;
    var nonceStr;
    var signature;
    var jsapi_ticket;
    var url;
    var isConfig = false;
    var initConfig = function () {
        var defered = $q.defer();
        if (!isConfig) {
            $myhttp.get('/nmjd/wechat/appid').then(function (result) {
                appid = result;
                var date = new Date();
                timestamp = date.getTime() + ""; // 必填，生成签名的时间戳
                timestamp = timestamp.substring(0, 10);
                return $myhttp.get('/nmjd/wechat/nonceStr');
            }).then(function (result) {
                nonceStr = result;
                return $myhttp.get('/nmjd/wechat/url');
            }).then(function (result) {
                url = result;
                return $myhttp.get('/nmjd/wechat/jsapi_ticket');
            }).then(function (result) {
                jsapi_ticket = result;
                //签名步骤一：按字典序排序参数
                var param = {jsapi_ticket: jsapi_ticket, noncestr: nonceStr, timestamp: timestamp, url: url};
                return $myhttp.get('/nmjd/wechat/signature', {param: param});
            }).then(function (result) {
                signature = result;
                wx.config({
                    debug: false, // 开启调试模式,调用的所有api的返回值会在客户端alert出来，若要查看传入的参数，可以在pc端打开，参数信息会通过log打出，仅在pc端时才会打印。
                    appId: appid, // 必填，企业号的唯一标识，此处填写企业号corpid
                    timestamp: timestamp, // 必填，生成签名的时间戳
                    nonceStr: nonceStr, // 必填，生成签名的随机串
                    signature: signature,// 必填，签名，见附录1
                    jsApiList: ['checkJsApi',
                        'getLocation',
                        'onMenuShareTimeline',
                        'onMenuShareAppMessage',
                        'onMenuShareQQ',
                        'onMenuShareWeibo',
                        'onMenuShareQZone',
                        'chooseImage',
                        'uploadImage',
                        'downloadImage'
                    ] // 必填，需要使用的JS接口列表，所有JS接口列表见附录2
                });
                wx.ready(function () {
                    isConfig = true;
                    defered.resolve();
                    // 1 判断当前版本是否支持指定 JS 接口，支持批量判断
                    /*wx.checkJsApi({
                     jsApiList: [
                         'checkJsApi',
                         'getLocation',
                         'onMenuShareTimeline',
                         'onMenuShareAppMessage',
                         'onMenuShareQQ',
                         'onMenuShareWeibo',
                         'onMenuShareQZone',
                         'chooseImage',
                         'uploadImage',
                         'downloadImage'
                     ],
                     success: function (res) {
                     alert('checkJsApi success='+JSON.stringify(res));
                     },
                     fail: function (res) {
                        alert('checkJsApi fail='+JSON.stringify(res));
                     }
                     });*/
                });
                wx.error(function(res){
                    alert('6.config fail='+JSON.stringify(res));// config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
                });
            });
        } else {
            defered.resolve();
        }
        return defered.promise;
    };

    service.getLatLng = function () {
        var defered = $q.defer();
        initConfig().then(function () {
            wx.getLocation({
                type: 'wgs84', // 默认为wgs84的gps坐标，如果要返回直接给openLocation用的火星坐标，可传入'gcj02'
                success: function (res) {
                    //alert("loc success return");
                    var r = {};
                    r.latitude = res.latitude; // 纬度，浮点数，范围为90 ~ -90
                    r.longitude = res.longitude; // 经度，浮点数，范围为180 ~ -180。
                    r.speed = res.speed; // 速度，以米/每秒计
                    r.accuracy = res.accuracy; // 位置精度
                    defered.resolve(r);
                },
                fail: function (err) {
                    alert("loc fail return");
                    defered.reject(err);
                },
                complete: function (com) {
                    //alert("loc complete return"+com);
                }
            });
        });
        return defered.promise;
    };

    service.myLocation = function (lat, lng) {
        var defered = $q.defer();
        $myhttp.get('/nmjd/wechat/location', {lat: lat, lng: lng}).then(function (result) {
            //alert(result);
            defered.resolve(result);
        }, function (e) {
            alert('myLocation:' + e);
            defered.reject(e);
        });
        return defered.promise;
    }

    service.getImage = function () {
        var defered = $q.defer();
        initConfig().then(function () {
            wx.chooseImage({
                count: 1, // 默认9
                sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
                sourceType: ['album', 'camera'], // 可以指定来源是相册还是相机，默认二者都有
                success: function (res) {
                    //alert(JSON.stringify(res));
                    var localIds = res.localIds; // 返回选定照片的本地ID列表，localId可以作为img标签的src属性显示图片
                    defered.resolve(localIds);
                },
                fail: function (res) {
                    defered.reject(res);
                }
            });
        })
        return defered.promise;
    }

    service.uploadImg = function (localId,fileName,code) {
        var defered = $q.defer();
        initConfig().then(function () {
            wx.uploadImage({
                localId: localId, // 需要上传的图片的本地ID，由chooseImage接口获得
                isShowProgressTips: 1, // 默认为1，显示进度提示
                success: function (res) {
                    var serverId = res.serverId; // 返回图片的服务器端ID
                    //alert('=='+serverId);//5
                    $myhttp.get('/nmjd/wechat/downloadImg',{code:code ,serverId:serverId,fileName:fileName}).then(function(){
                        defered.resolve(serverId);
                    });
                },
                fail: function (res) {
                    defered.reject(res);
                }
            });
        })
        return defered.promise;
    }

    service.pviewImages = function(currentImg,imgList){
        initConfig().then(function () {
            wx.previewImage({
                current: currentImg, // 当前显示图片的http链接
                urls: imgList // 需要预览的图片http链接列表
            });
        });
    }

    service.downloadImg = function (serverId) {
        var defered = $q.defer();
        initConfig().then(function () {
            wx.downloadImage({
                serverId: serverId, // 需要下载的图片的服务器端ID，由uploadImage接口获得
                isShowProgressTips: 1, // 默认为1，显示进度提示
                success: function (res) {
                    var localId = res.localId; // 返回图片下载后的本地ID
                    defered.resolve(localId);
                },
                fail: function (res) {
                    defered.reject(res);
                }
            });
        })
        return defered.promise;
    }

    return service;
})


app.factory('$wechatPay',function($q,$myhttp){
    var service = {};

    service.callpay = function(params,code){
        var defered = $q.defer();
        $myhttp.get('nmjd/wechat_pay/appid').then(function(appid){
            params.appid = appid;
            return $myhttp.get('nmjd/wechat_pay/mch_id');
        }).then(function(mch_id) {
            params.mch_id = mch_id;
            return $myhttp.get('/nmjd/wechat_pay/spbill_create_ip');
        }).then(function(spbill_create_ip){//终端ip
            params.spbill_create_ip = spbill_create_ip;
            params.trade_type = "JSAPI";
            return $myhttp.get('/nmjd/wechat_pay/pay_params',{params:params});
        }).then(function(r){//alert(JSON.stringify(r));
            WeixinJSBridge.invoke(
                'getBrandWCPayRequest', {
                    "appId" : r.appId,     //公众号名称，由商户传入
                    "timeStamp":r.timeStamp,         //时间戳，自1970年以来的秒数
                    "nonceStr" : r.nonceStr, //随机串
                    "package" : r.package,
                    "signType" : "MD5",         //微信签名方式:
                    "paySign" : r.paySign //微信签名
                },
                     function(res){
                     //2#支付验证签名失败#get_brand_wcpay_request:fail
                     //alert(res.err_code+'#'+res.err_desc+'#'+res.err_msg);
                     defered.resolve(res);
                     /*if(res.err_msg == "get_brand_wcpay_request:ok"){
                     //判断前端返回，微信团队郑重提示：res.err_msg将在用户支付成功后返回ok，但并不保证它绝对可靠
                     var r = {};
                     r.return_code = 'ok';
                     defered.resolve(r);
                     } else{
                        defered.reject(res);
                    }*/

                }
            );
        })
        return defered.promise;
    }
    return service;
})
