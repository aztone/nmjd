/**
 * Created by wing on 2014/8/21.
 */
'use strict';
var app = angular.module('app');

app.controller('JdqjMainCtrl', function ($scope, $state, $stateParams) {
    //alert($stateParams.code);
    $scope.code = $stateParams.code;
});

app.controller('JdqjOrderCtrl', function ($scope, $state, $stateParams, $myhttp, $modal, CommonService, UserService, $window ,$wcjssdk ,CommonDataService, $wechatPay) {
    $scope.code = $stateParams.code;

    $scope.card_mes = '';
    $scope.car_model_mes = undefined;
    $scope.user_info_mes = undefined;
    $scope.service_at_mes = undefined;
    $scope.FlagMainPage = true;
    $scope.FlagContactInput = false;
    $scope.FlagPayPage = false;
    $scope.PayButton = false;//用于限制重复点击
    $scope.contactway = '';

    //微信支付初始化
    $scope.openid = '';
    var params = {body:"柠檬家电"};//商品描述
    //params.total_fee = 1;//总金额 单位为分，不能带小数点
    params.notify_url = "http://gd.efu5.com/nmjd_pay/notify/notify_url_qxj";//接收微信支付成功通知

    $scope.qxj = {deductible_amount:0,price:268,number:1,sheetType:'B',name:"",tel:"",address:""};
    $scope.price = 268;

        //数量增减操作
    $scope.changeNumber = function(type){
        if(type=='A')
            $scope.qxj.number += 1;
        else if(type=='M'&&$scope.qxj.number>1)
            $scope.qxj.number -= 1;
        $scope.price = $scope.qxj.number*268;
        $scope.qxj.price = $scope.price - $scope.qxj.deductible_amount;
    };

    //查询清洁卡号是否有效
    $scope.findCard = function(){
        if($scope.qxj.claeanCard){
            $myhttp.get('/nmjd/card/'+$scope.qxj.claeanCard).then(function (result) {
                if(result){
                    $scope.qxj.deductible_amount = result.deductible_amount;
                    $scope.qxj.price = $scope.price - $scope.qxj.deductible_amount;
                    $scope.card_mes = '';
                }else{
                    $scope.qxj.deductible_amount = 0;
                    $scope.qxj.price = $scope.price - $scope.qxj.deductible_amount;
                    $scope.card_mes = '注册码不存在';
                }
            });
        }
    };

    $scope.contactwayShow = function(){
        $scope.FlagContactInput=true;
        $scope.FlagMainPage=false;
        $scope.FlagPayPage=false;
    }
    //联系方式填写确定
    $scope.contactWayComfirm = function(){
        if($scope.qxj.name && $scope.qxj.tel && $scope.qxj.address){
            $scope.contactway = $scope.qxj.name + '   ' + $scope.qxj.tel + '   ' + $scope.qxj.address;
            $scope.user_info_mes = undefined;
        }else{
            if(!$scope.qxj.name && !$scope.qxj.tel && !$scope.qxj.address)
                $scope.contactway = '无';
            else
                $scope.contactway = $scope.qxj.name + '   ' + $scope.qxj.tel + '   ' + $scope.qxj.address;
            $scope.user_info_mes = '请完善联系方式';
        }
        $scope.FlagContactInput = false;
        $scope.FlagMainPage = true;
    }

    //提交订单：判断输入内容 显示待支付页面
    $scope.sheetCommit = function(){
        if(!$scope.qxj.car_model)
            $scope.car_model_mes = '请填写车型';
        else
            $scope.car_model_mes = undefined;

        if($scope.qxj.name && $scope.qxj.tel && $scope.qxj.address)
            $scope.user_info_mes = undefined;
        else
            $scope.user_info_mes = '请完善联系方式';

        if(!$scope.qxj.service_at)
            $scope.service_at_mes = '请选择预约时间';
        else
            $scope.service_at_mes = undefined;

        if($scope.qxj.number > 0 && $scope.qxj.car_model && $scope.qxj.name && $scope.qxj.tel && $scope.qxj.address && $scope.qxj.service_at){
            $scope.FlagMainPage=false;
            $scope.FlagPayPage=true;
        }
    };

    //确认支付
    $scope.pay = function(){
        $scope.PayButton = true;
        if($scope.qxj.price > 0){
            $scope.realpay();
        }else{
            $scope.registe();
        }
    };

    $scope.registe = function(){
        $scope.qxj.state = 'N';//现在并未注册成功
        $scope.qxj.total_amount = $scope.price;//现在并未注册成功
        console.log($scope.qxj);
        $myhttp.post('/nmjd/sheet/register', {qxj: $scope.qxj}).then(function (result) {
            alert('注册成功');
            //$state.go('qxj.qxj-choose');
            return $myhttp.get('/nmjd/wechat/gourl');
        }).then(function(url){
            window.location = url;
        }), function (err) {
            alert(err);
        }
    };

    $scope.realpay = function(){
        $scope.qxj.state = 'N';//现在并未注册成功
        $scope.qxj.total_amount = $scope.price;//现在并未注册成功
        console.log($scope.qxj);

        $myhttp.post('/nmjd/sheet/register', {qxj: $scope.qxj}).then(function (result) {
            params.out_trade_no = result._id;//将商品
            params.total_fee =  $scope.qxj.price*100;//总金额 单位为分，不能带小数点，设置支付金额
            return $myhttp.get('/nmjd/wechat_pay/openid',{code:$scope.code});
        }).then(function(openid) {
            params.openid = openid;
            $scope.openid = openid;
            $wechatPay.callpay(params,$scope.code).then(function(r){
                if(r.err_msg == "get_brand_wcpay_request:ok"){
                    alert('支付成功');
                    //$state.go('qxj.qxj-choose')
                    $myhttp.get('/nmjd/wechat/gourl').then(function(url) {
                        window.location = url;
                    });
                }else if(r.err_msg == "get_brand_wcpay_request:cancel"){
                    alert('注册成功，支付取消，请重新支付');
                    $scope.PayButton = false;
                }else if(r.err_msg == "get_brand_wcpay_request:fail"){
                    alert('注册成功，支付失败，请重新尝试');
                    $scope.PayButton = false;
                }
            });
        })
    };

});