/**
 * Created by hw on 2014/8/25.
 */
'use strict';
var app = angular.module('app');
app.controller('MetadataListCtrl', function ($scope, CommonService, $modal, $myhttp) {
    $scope.metadata = {};

    $myhttp.get('/api/metadata/metadata.context').then(function (result) {
        $scope.contextList = result;
    }, function (err) {
        CommonService.error(err);
    });
    /*搜索*/
    $scope.search = function (context) {
        if (context) {
            $scope.metadata.key = context;
            $myhttp.get('/api/metadata/' + context).then(function (result) {
                $scope.metadatas = result;
            }).then(function () {
                if (context == 'metadata.context') {
                    $myhttp.put('/api/metadata/552f57eb7deb212c2642f0f7', {metadata: {context: 'metadata.context', value: '可维护数据项', key: 'metadata.context', state: 'Y'}}).then(function (result) {
                    }, function (err) {
//                        CommonService.error(err);
                    });
                }
            });
        } else {
            $myhttp.get('/api/metadata/' + $scope.metadata.key).then(function (result) {
                if ($scope.metadata.state) {
                    $scope.metadatas = _.filter(result, {state: 'N'});
                } else
                    $scope.metadatas = _.filter(result, {state: 'Y'});
            }, function (err) {
                CommonService.error(err);
            });
        }
    }
    /*停用启用*/
    $scope.startOrStop = function (meta, state) {
        $myhttp.put('/api/metadata/' + meta._id, {metadata: {context: meta.context, value: meta.value, state: state}}).then(function (result) {
            CommonService.success("操作成功");
            $scope.search();
        }, function (err) {
            CommonService.error(err);
        });
    }

    $scope.$watch('metadata.key', function () {
        if ($scope.metadata.key != undefined || $scope.metadata.key != '') {
            $scope.search();
        }
    });

    $scope.$watch('metadata.state', function () {
        if ($scope.metadata.key) {
            $myhttp.get('/api/metadata/' + $scope.metadata.key).then(function (result) {
                if ($scope.metadata.state) {
                    $scope.metadatas = _.filter(result, {state: 'N'});
                } else
                    $scope.metadatas = _.filter(result, {state: 'Y'});
            }, function (err) {
                CommonService.error(err);
            });
        }
    });

    $scope.openDetailModal = function (size, spr, key) {
        var modalInstance = $modal.open({
            templateUrl: 'metadata_detail.html',
            controller: function ($scope, $modalInstance, $myhttp) {
                $scope.meta = {};
                $myhttp.get('/api/metadata/metadata.context').then(function (result) {
                    $scope.contextNewList = result;
                    $scope.refContextList = result;
                    if (!spr) {
                        $scope.meta.context = key || "";
                    } else {
                        $scope.meta = spr;
                    }
                }, function (err) {
                    CommonService.error(err);
                });



                $scope.$watch('meta.context', function () {
                    if (!$scope.meta._id) {
                        if ($scope.meta.context != undefined && $scope.meta.context != 'metadata.context' && $scope.meta.context != 'policy.life') {
                            $myhttp.get('/api/metadata/' + $scope.meta.context).then(function (result) {
                                $scope.meta.key = String.fromCharCode((65 + result.length));
                            }, function (err) {
                                CommonService.error(err);
                            });
                        } else {
                            $scope.meta.key = "";
                        }
                    }
                });

                $scope.$watch('meta.ref_context', function () {
                    if ($scope.meta.ref_context) {
                        $myhttp.get('/api/metadata/' + $scope.meta.ref_context).then(function (result) {
                            $scope.keyList = result;
                        }, function (err) {
                            CommonService.error(err);
                        });
                    }
                });

                $scope.cancel = function () {
                    $modalInstance.dismiss('cancel');
                };

                $myhttp.get('/api/metadata/metadata.context').then(function (result) {
                    $scope.contextNewList = result;
                    if (key) {
                        $scope.metadata = {key: key};
                    }
                }, function (err) {
                    CommonService.error(err);
                });

                $scope.add = function (meta_new) {
                    $myhttp.post('/api/metadata', {metadata: meta_new}).then(function (result) {
                        CommonService.success("操作成功");
                        $scope.cancel();
                    }, function (err) {
                        CommonService.error(err);
                    });
                };
                $scope.save = function (item) {
                    $myhttp.put('/api/metadata/' + item._id, {metadata: item}).then(function (result) {
                        CommonService.success("操作成功");
                        $scope.cancel();
                    }, function (err) {
                        CommonService.error(err);
                    });
                }
            },
            size: size
        });


        //关联项
        $scope.showFlag = false;
        $scope.changeShow= function () {
            if($scope.showFlag){
                $scope.showFlag =false;
            }else{
                $scope.showFlag = true;
            }
        }



        modalInstance.result.then(function (user) {
        }, function () {
            $scope.search();
        });
    };

    //初始化
    $scope.metadata.key = 'product.brand';

});
