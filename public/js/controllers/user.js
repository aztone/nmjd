/**
 * Created by wing on 2015/4/28.
 */
'use strict';
var app = angular.module('app');
app.controller('UserManageCtrl', function ($scope, $state, $stateParams, $myhttp, $modal, CommonService, UserService) {
    $scope.users;
    $scope.parentTerminal;
    $scope.userTerminal;
    $scope.terminalsGroup = [];


    $myhttp.get('/api/terminal/' + UserService.user.terminal).then(function (terminal) {
        $scope.userTerminal = terminal;
        $scope.parentTerminal = terminal;
        pickSon(UserService.user.terminal);
    });

    var pickSon = function (parent) {
        $myhttp.get('/api/terminal/son/' + parent).then(function (result) {
            if (result.length > 0) {
                $scope.terminalsGroup.push({terminals: result});
            }
        }, function (err) {
            CommonService.error(err);
        });
    };

    $scope.terminalChange = function (index) {
        if ($scope.terminalsGroup[index].selected) {
            $scope.terminalsGroup = $scope.terminalsGroup.splice(0, index + 1);
            $scope.parentTerminal = $scope.terminalsGroup[index].selected;
        } else {
            $scope.terminalsGroup = $scope.terminalsGroup.splice(0, index);
            if (index > 0) {
                $scope.parentTerminal = $scope.terminalsGroup[index - 1].selected;
            } else {
                $scope.parentTerminal = $scope.userTerminal;
            }
        }
        pickSon($scope.parentTerminal._id);
        $scope.getUsers($scope.parentTerminal._id);
    };

    $scope.getUsers = function (_id) {
        $myhttp.get('/api/user', {
            where: {
                terminal: _id
            }
        }).then(function (users) {
            $scope.users = users;
        })
    };
    $scope.getUsers(UserService.user.terminal);


    $scope.addUser = function () {
        var parentTerminal = $scope.parentTerminal;
        $modal.open({
            size:'md',
            templateUrl: 'views/manage/user_add.html',
            controller: function($scope, $modalInstance){
                $scope.parentTerminal = parentTerminal;
                $scope.user = {terminal: parentTerminal._id};
                $myhttp.get('/api/feature').then(function (data) {
                    $scope.features = [];
                    _.forEach(data, function (d) {
                        if (_.contains(UserService.user.features, d.id)){
                            $scope.features.push(d);
                        }
                    });
                });
                $scope.ok = function () {
                    $scope.user.password = $.md5($scope.user.passwd);
                    $scope.user.features = _.pluck(_.filter($scope.features, {checked: true}), 'id');
                    $myhttp.post('/api/user/' + $scope.parentTerminal._id, {user: $scope.user}).then(function () {
                        CommonService.success('添加成功');
                        $modalInstance.close();
                    })
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('close');
                }
            }
        }).result.then(function () {
                $state.reload();
            })
    };
    $scope.editUser = function (user) {
        var parentTerminal = $scope.parentTerminal;
        $modal.open({
            size:'md',
            templateUrl: 'views/manage/user_edit.html',
            controller: function($scope, $modalInstance){
                $scope.parentTerminal = parentTerminal;
                $scope.user = user;

                $myhttp.get('/api/feature').then(function (data) {
                    $scope.features = [];
                    _.forEach(data, function (d) {
                        if (_.contains(UserService.user.features, d.id)){
                            $scope.features.push(d);
                        }
                    });
                    _.forEach($scope.features, function (feature) {
                        feature.checked = _.contains($scope.user.features, feature.id);
                    })
                });
                $scope.ok = function () {
                    $scope.user.password = $scope.user.passwd ? $.md5($scope.user.passwd) : undefined;
                    $scope.user.features = _.pluck(_.filter($scope.features, {checked: true}), 'id');
                    $myhttp.put('/api/user/' + $scope.user._id, {user: $scope.user}).then(function () {
                        CommonService.success('保存成功');
                        $modalInstance.close();
                    })
                };

                $scope.cancel = function () {
                    $modalInstance.dismiss('close');
                }
            }
        }).result.then(function () {
                $state.reload();
            })
    }

});