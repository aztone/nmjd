'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngAnimate',
    'ngCookies',
    'ngStorage',
    'ngSanitize',
    'angularFileUpload',
    'ui.router',
    'ui.bootstrap',
    'ui.load',
    'ui.jq',
    'ui.validate',
    'ui.select',
    'pascalprecht.translate',
    'app.filters',
    'app.services',
    'app.directives',
    'app.controllers'
]);

app.run(['$rootScope', '$state', '$stateParams', 'UserService', '$location', '$myhttp', '$log', '$localStorage',
    function ($rootScope, $state, $stateParams, UserService, $location, $myhttp, $log, $localStorage) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;
        $rootScope.$on('$stateChangeStart', function (event, toState, toParams) {
            console.log(toState);//alert(toState.name+JSON.stringify(toParams));
            if (toState.name.indexOf('access') > 0 || toState.name.indexOf('app') > 0) {
                return $myhttp.get('/access/info').then(function (result) {
                    if (!result) {
                        $location.path("/access/signin");
                    } else {
                        UserService.user = result;
                    }
                }, function () {
                    $location.path("/access/signin");
                });
            }
        });
    }
]);

app.config(['$stateProvider', '$urlRouterProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide',
    function ($stateProvider, $urlRouterProvider, $controllerProvider, $compileProvider, $filterProvider, $provide) {
        // lazy controller, directive and service
        app.controller = $controllerProvider.register;
        app.directive = $compileProvider.directive;
        app.filter = $filterProvider.register;
        app.factory = $provide.factory;
        app.service = $provide.service;
        app.constant = $provide.constant;
        app.value = $provide.value;

        $urlRouterProvider.otherwise('/nmjd/jdqj/main');
        $stateProvider.state('app', {
            abstract: true,
            url: '/app',
            templateUrl: 'views/app.html'
        }).state('app.dashboard', {
            url: '/dashboard',
            templateUrl: 'views/app_dashboard.html'
        }).state('app.manage', {
            url: '/manage',
            template: '<div ui-view class="fade-in-up"></div>'
        }).state('app.manage.user', {
            url: '/user',
            templateUrl: 'views/manage/user.html',
            resolve: {
                deps: ['uiLoad', function (uiLoad) {
                    return uiLoad.load('js/jquery/jquery.md5.js');
                }]
            }
        }).state('access', {
            url: '/access',
            template: '<div ui-view class="fade-in-right-big smooth"></div>'
        }).state('access.signin', {
            url: '/signin',
            templateUrl: 'views/access/signin.html',
            resolve: {
                deps: ['uiLoad', function (uiLoad) {
                    return uiLoad.load('js/jquery/jquery.md5.js');
                }]
            }
        }).state('app.user', {
            url: '/user',
            template: '<div ui-view class="fade-in-up"></div>'
        }).state('app.user.settings', {
            url: '/settings',
            templateUrl: 'views/user/settings.html',
            resolve: {
                deps: ['uiLoad', function (uiLoad) {
                    return uiLoad.load('js/jquery/jquery.md5.js');
                }]
            }
        }).state('app.system', {
            url: '/system',
            template: '<div ui-view class=""></div>'
        }).state('app.system.metadata', {
            url: '/metadata',
            templateUrl: 'views/system/metadata.html'
        }).state('nmjd', {//柠檬家电
            url: '/nmjd',
            template: '<div ui-view class=""></div>'
        }).state('nmjd.jdqj', {//柠檬家电 家电清洁
            url: '/jdqj',
            template: '<div ui-view class=""></div>'
        }).state('nmjd.jdqj.main', {//家电清洁主页
            url: '/main?code',
            templateUrl: 'views/jdqj/main.html'
        }).state('nmjd.jdqj.car_clean', {//汽车空调上门清洁
            url: '/car_clean?code',
            templateUrl: 'views/jdqj/car_clean.html'
        }).state('nmjd.jdqj.carclean_order', {
            url: '/carclean_order?code',
            templateUrl: 'views/jdqj/carclean_order.html'
        })
    }
]);

app.config(['$translateProvider', function ($translateProvider) {
    // Register a loader for the static files
    // So, the module will search missing translation tables under the specified urls.
    // Those urls are [prefix][langKey][suffix].
    $translateProvider.useStaticFilesLoader({
        prefix: 'l10n/',
        suffix: '.json'
    });

    // Tell the module what language to use by default
    $translateProvider.preferredLanguage('zh_CN');

    // Tell the module to store the language in the local storage
    $translateProvider.useLocalStorage();
}]);

/**
 * jQuery plugin config use ui-jq directive , config the js and css files that required
 * key: function name of the jQuery plugin
 * value: array of the css js file located
 */
app.constant('JQ_CONFIG', {
    easyPieChart: ['js/jquery/charts/easypiechart/jquery.easy-pie-chart.js'],
    sparkline: ['js/jquery/charts/sparkline/jquery.sparkline.min.js'],
    plot: [
        'js/jquery/charts/flot/jquery.flot.min.js',
        'js/jquery/charts/flot/jquery.flot.resize.js',
        'js/jquery/charts/flot/jquery.flot.tooltip.min.js',
        'js/jquery/charts/flot/jquery.flot.spline.js',
        'js/jquery/charts/flot/jquery.flot.orderBars.js',
        'js/jquery/charts/flot/jquery.flot.pie.min.js'
    ],
    slimScroll: ['js/jquery/slimscroll/jquery.slimscroll.min.js'],
    sortable: ['js/jquery/sortable/jquery.sortable.js'],
    nestable: [
        'js/jquery/nestable/jquery.nestable.js',
        'js/jquery/nestable/nestable.css'
    ],
    filestyle: ['js/jquery/file/bootstrap-filestyle.min.js'],
    slider: [
        'js/jquery/slider/bootstrap-slider.js',
        'js/jquery/slider/slider.css'
    ],
    chosen: [
        'js/jquery/chosen/chosen.jquery.min.js',
        'js/jquery/chosen/chosen.css'
    ],
    TouchSpin: [
        'js/jquery/spinner/jquery.bootstrap-touchspin.min.js',
        'js/jquery/spinner/jquery.bootstrap-touchspin.css'
    ],
    wysiwyg: [
        'js/jquery/wysiwyg/bootstrap-wysiwyg.js',
        'js/jquery/wysiwyg/jquery.hotkeys.js'
    ],
    dataTable: [
        'js/jquery/datatables/jquery.dataTables.min.js',
        'js/jquery/datatables/dataTables.bootstrap.js',
        'js/jquery/datatables/dataTables.bootstrap.css'
    ],
    vectorMap: [
        'js/jquery/jvectormap/jquery-jvectormap.min.js',
        'js/jquery/jvectormap/jquery-jvectormap-world-mill-en.js',
        'js/jquery/jvectormap/jquery-jvectormap-us-aea-en.js',
        'js/jquery/jvectormap/jquery-jvectormap.css'
    ],
    footable: [
        'js/jquery/footable/footable.all.min.js',
        'js/jquery/footable/footable.core.css'
    ]
});
app.constant('MODULE_CONFIG', {
    select2: [
        'js/jquery/select2/select2.css',
        'js/jquery/select2/select2-bootstrap.css',
        'js/jquery/select2/select2.min.js',
        'js/modules/ui-select2.js'
    ]
});