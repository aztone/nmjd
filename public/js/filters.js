'use strict';

/* Filters */
// need load the moment.js to use this filter. 
var app = angular.module('app.filters', []);
app.filter('fromNow', function () {
    return function (date) {
        return moment(date).fromNow();
    }
});

app.filter('transMetadata', function () {
    return function (metadatas, context, key) {

        var metadata = _.find(metadatas, {context: context, key: key});
        return metadata ? metadata.value : '';
    }
});
