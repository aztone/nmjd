'use strict';

/* Directives */
// All the directives rely on jQuery.

var app = angular.module('app.directives', ['ui.load']);
app.directive('uiModule', ['MODULE_CONFIG', 'uiLoad', '$compile', function (MODULE_CONFIG, uiLoad, $compile) {
    return {
        restrict: 'A',
        compile: function (el, attrs) {
            var contents = el.contents().clone();
            return function (scope, el, attrs) {
                el.contents().remove();
                uiLoad.load(MODULE_CONFIG[attrs.uiModule])
                    .then(function () {
                        $compile(contents)(scope, function (clonedElement, scope) {
                            el.append(clonedElement);
                        });
                    });
            }
        }
    };
}]);
app.directive('uiShift', ['$timeout', function ($timeout) {
    return {
        restrict: 'A',
        link: function (scope, el, attr) {
            // get the $prev or $parent of this el
            var _el = $(el),
                _window = $(window),
                prev = _el.prev(),
                parent,
                width = _window.width()
                ;

            !prev.length && (parent = _el.parent());

            function sm() {
                $timeout(function () {
                    var method = attr.uiShift;
                    var target = attr.target;
                    _el.hasClass('in') || _el[method](target).addClass('in');
                });
            }

            function md() {
                parent && parent['prepend'](el);
                !parent && _el['insertAfter'](prev);
                _el.removeClass('in');
            }

            (width < 768 && sm()) || md();

            _window.resize(function () {
                if (width !== _window.width()) {
                    $timeout(function () {
                        (_window.width() < 768 && sm()) || md();
                        width = _window.width();
                    });
                }
            });
        }
    };
}]);
app.directive('uiToggleClass', ['$timeout', '$document', function ($timeout, $document) {
    return {
        restrict: 'AC',
        link: function (scope, el, attr) {
            el.on('click', function (e) {
                e.preventDefault();
                var classes = attr.uiToggleClass.split(','),
                    targets = (attr.target && attr.target.split(',')) || Array(el),
                    key = 0;
                angular.forEach(classes, function (_class) {
                    var target = targets[(targets.length && key)];
                    ( _class.indexOf('*') !== -1 ) && magic(_class, target);
                    $(target).toggleClass(_class);
                    key++;
                });
                $(el).toggleClass('active');

                function magic(_class, target) {
                    var patt = new RegExp('\\s' +
                    _class.
                        replace(/\*/g, '[A-Za-z0-9-_]+').
                        split(' ').
                        join('\\s|\\s') +
                    '\\s', 'g');
                    var cn = ' ' + $(target)[0].className + ' ';
                    while (patt.test(cn)) {
                        cn = cn.replace(patt, ' ');
                    }
                    $(target)[0].className = $.trim(cn);
                }
            });
        }
    };
}]);
app.directive('uiNav', ['$timeout', function ($timeout) {
    return {
        restrict: 'AC',
        link: function (scope, el, attr) {
            var _window = $(window),
                _mb = 768,
                wrap = $('.app-aside'),
                next,
                backdrop = '.dropdown-backdrop';
            // unfolded
            el.on('click', 'a', function (e) {
                next && next.trigger('mouseleave.nav');
                var _this = $(this);
                _this.parent().siblings(".active").toggleClass('active');
                _this.next().is('ul') && _this.parent().toggleClass('active') && e.preventDefault();
                // mobile
                _this.next().is('ul') || ( ( _window.width() < _mb ) && $('.app-aside').removeClass('show off-screen') );
            });

            // folded & fixed
            el.on('mouseenter', 'a', function (e) {
                next && next.trigger('mouseleave.nav');
                $('> .nav', wrap).remove();
                if (!$('.app-aside-fixed.app-aside-folded').length || ( _window.width() < _mb )) return;
                var _this = $(e.target)
                    , top
                    , w_h = $(window).height()
                    , offset = 50
                    , min = 150;

                !_this.is('a') && (_this = _this.closest('a'));
                if (_this.next().is('ul')) {
                    next = _this.next();
                } else {
                    return;
                }

                _this.parent().addClass('active');
                top = _this.parent().position().top + offset;
                next.css('top', top);
                if (top + next.height() > w_h) {
                    next.css('bottom', 0);
                }
                if (top + min > w_h) {
                    next.css('bottom', w_h - top - offset).css('top', 'auto');
                }
                next.appendTo(wrap);

                next.on('mouseleave.nav', function (e) {
                    $(backdrop).remove();
                    next.appendTo(_this.parent());
                    next.off('mouseleave.nav').css('top', 'auto').css('bottom', 'auto');
                    _this.parent().removeClass('active');
                });

                $('.smart').length && $('<div class="dropdown-backdrop"/>').insertAfter('.app-aside').on('click', function (next) {
                    next && next.trigger('mouseleave.nav');
                });

            });

            wrap.on('mouseleave', function (e) {
                next && next.trigger('mouseleave.nav');
                $('> .nav', wrap).remove();
            });
        }
    };
}]);
app.directive('uiScroll', ['$location', '$anchorScroll', function ($location, $anchorScroll) {
    return {
        restrict: 'AC',
        link: function (scope, el, attr) {
            el.on('click', function (e) {
                $location.hash(attr.uiScroll);
                $anchorScroll();
            });
        }
    };
}]);
app.directive('uiFullscreen', ['uiLoad', function (uiLoad) {
    return {
        restrict: 'AC',
        template: '<i class="fa fa-expand fa-fw text"></i><i class="fa fa-compress fa-fw text-active"></i>',
        link: function (scope, el, attr) {
            el.addClass('hide');
            uiLoad.load('js/libs/screenfull.min.js').then(function () {
                if (screenfull.enabled) {
                    el.removeClass('hide');
                }
                el.on('click', function () {
                    var target;
                    attr.target && ( target = $(attr.target)[0] );
                    el.toggleClass('active');
                    screenfull.toggle(target);
                });
            });
        }
    };
}]);
app.directive('uiButterbar', ['$rootScope', '$anchorScroll', function ($rootScope, $anchorScroll) {
    return {
        restrict: 'AC',
        template: '<span class="bar"></span>',
        link: function (scope, el, attrs) {
            el.addClass('butterbar hide');
            scope.$on('$stateChangeStart', function (event) {
                $anchorScroll();
                el.removeClass('hide').addClass('active');
            });
            scope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState) {
                event.targetScope.$watch('$viewContentLoaded', function () {
                    el.addClass('hide').removeClass('active');
                })
            });
        }
    };
}]);
app.directive('setNgAnimate', ['$animate', function ($animate) {
    return {
        link: function ($scope, $element, $attrs) {
            $scope.$watch(function () {
                return $scope.$eval($attrs.setNgAnimate, $scope);
            }, function (valnew, valold) {
                $animate.enabled(!!valnew, $element);
            });
        }
    };
}]);


app.directive('datetimepicker', function () {
    return {
        restrict: 'AE',
        link: function (scope, elm) {
            $(elm).datetimepicker({
                "lang": "ch",
                'format': 'Y-m-d H:i',
                'closeOnDateSelect': true,
                step: 5,
                mask: true,
                scrollInput: false
            });
        }
    }
});

app.directive('mytimepicker', function () {
    return {
        restrict: 'AE',
        link: function (scope, elm) {
            $(elm).datetimepicker({
                lang: "ch",
                datepicker: false,
                format: 'H:i',
                closeOnDateSelect: true,
                step: 30,
                mask: true,
                scrollInput: false
            });
        }
    }
});

app.directive('mydatepicker', function () {
    return {
        restrict: 'AE',
        link: function (scope, elm) {
            $(elm).datetimepicker({
                lang: "ch",
                timepicker: false,
                format: 'Y-m-d',
                closeOnDateSelect: true,
                mask: true,
                scrollInput: false
            });
        }
    }
});

app.directive('pca', function () {
    return {
        restrict: 'AE',
        scope: {
            ngProvince: '=',
            ngCity: '=',
            ngArea: '='
        },
        templateUrl: 'views/blocks/province_city_area.html',
        controller: function ($scope, $myhttp) {
            var flag = {city: true, area: true};

            $myhttp.get('/api/region/province').then(function (provinces) {
                $scope.provinces = provinces;
            });

            $scope.$watch('ngProvince', function () {
                if ($scope.ngProvince) {
                    $myhttp.get('/api/region/city/' + $scope.ngProvince).then(function (cities) {
                        $scope.cities = cities;
                        if (!flag.city) {
                            $scope.ngCity = "";
                            $scope.ngArea = "";
                            flag.city = false;
                        }
                        $scope.areas = [];
                    })
                }
            });
            $scope.$watch('ngCity', function () {
                if ($scope.ngCity) {
                    $myhttp.get('/api/region/area/' + $scope.ngCity).then(function (areas) {
                        $scope.areas = areas;
                        if (!flag.area) {
                            $scope.ngArea = "";
                            flag.area = false;
                        }
                    })
                }
            });
        }
    }
});

app.directive('wechatpca', function () {
    return {
        restrict: 'AE',
        scope: {
            ngProvince: '=',
            ngCity: '=',
            ngArea: '='
        },
        templateUrl: 'views/blocks/province_city_area1.html',
        controller: function ($scope, $myhttp) {
            var flag = {city: true, area: true};

            $myhttp.get('/axb/axb/province').then(function (provinces) {
                $scope.provinces = provinces;
            });

            $scope.$watch('ngProvince', function () {
                if ($scope.ngProvince) {
                    $myhttp.get('/axb/axb/city/' + $scope.ngProvince).then(function (cities) {
                        $scope.cities = cities;
                        if (!flag.city) {
                            $scope.ngCity = "";
                            $scope.ngArea = "";
                            flag.city = false;
                        }
                        $scope.areas = [];
                    })
                }
            });
            $scope.$watch('ngCity', function () {
                if ($scope.ngCity) {
                    $myhttp.get('/axb/axb/area/' + $scope.ngCity).then(function (areas) {
                        $scope.areas = areas;
                        if (!flag.area) {
                            $scope.ngArea = "";
                            flag.area = false;
                        }
                    })
                }
            });
        }
    }
});

app.directive('wechatpca2', function () {
    return {
        restrict: 'AE',
        scope: {
            ngProvince: '=',
            ngCity: '=',
            ngArea: '='
        },
        templateUrl: 'views/blocks/province_city_area2.html',
        controller: function ($scope, $myhttp) {
            var flag = {city: true, area: true};

            $myhttp.get('/axb/axb/province').then(function (provinces) {
                $scope.provinces = provinces;
            });

            $scope.$watch('ngProvince', function () {
                if ($scope.ngProvince) {
                    $myhttp.get('/axb/axb/city/' + $scope.ngProvince).then(function (cities) {
                        $scope.cities = cities;
                        if (!flag.city) {
                            $scope.ngCity = "";
                            $scope.ngArea = "";
                            flag.city = false;
                        }
                        $scope.areas = [];
                    })
                }
            });
            $scope.$watch('ngCity', function () {
                if ($scope.ngCity) {
                    $myhttp.get('/axb/axb/area/' + $scope.ngCity).then(function (areas) {
                        $scope.areas = areas;
                        if (!flag.area) {
                            $scope.ngArea = "";
                            flag.area = false;
                        }
                    })
                }
            });
        }
    }
});

app.directive('spca', function () {
    return {
        restrict: 'AE',
        scope: {
            ngProvince: '@',
            ngCity: '@',
            ngArea: '@'
        },
        templateUrl: 'views/blocks/static_province_city_area.html',
        controller: function ($scope, $myhttp) {

            $scope.$watch('ngProvince', function () {
                if ($scope.ngProvince) {
                    $myhttp.get('/api/region/' + $scope.ngProvince).then(function (name) {
                        $scope.provinceName = name;
                    });
                }
            });
            $scope.$watch('ngCity', function () {
                if ($scope.ngCity) {
                    $myhttp.get('/api/region/' + $scope.ngCity).then(function (name) {
                        $scope.cityName = name;
                    });
                }
            });
            $scope.$watch('ngArea', function () {
                console.log($scope.ngArea);
                if ($scope.ngArea) {
                    $myhttp.get('/api/region/' + $scope.ngArea).then(function (name) {
                        $scope.areaName = name;
                    });
                }
            });
        }
    }
});

app.directive('confirm', function () {
    return {
        restrict: 'AE',
        scope: {
            ngConfirm: '&',
            ngCancel: '&'
        },
        link: function (scope, element) {
            $(element).click(function () {
                scope.click();
            })
        },
        controller: function ($scope, $modal) {
            $scope.click = function () {
                $modal.open({
                    size: 'sm',
                    templateUrl: 'views/blocks/confirm.html',
                    controller: function ($scope, $modalInstance) {
                        $scope.ok = function () {
                            $modalInstance.close();
                        };
                        $scope.cancel = function () {
                            $modalInstance.dismiss('close');
                        }
                    }
                }).result.then(function () {
                        console.log($scope.ngConfirm);
                        if ($scope.ngConfirm) {
                            $scope.ngConfirm();
                        }
                    }, function () {
                        if ($scope.ngCancel) {
                            $scope.ngCancel();
                        }
                    })
            };

        }
    }
});

app.directive('upload', function () {
    return {
        restrict: 'AE',
        require: '^ngModel',
        scope: {
            ngModel: '=',
            uploadType: '@'
        },
        templateUrl: 'views/blocks/upload.html',
        controller: function ($scope, $myhttp, FileUploader, CommonService) {
            var uploader = $scope.uploader = new FileUploader({url: '/api/upload/' + $scope.uploadType});
            $scope.upload = function () {
                uploader.uploadAll();
            };

            uploader.onSuccessItem = function (item, response) {
                $scope.ngModel = response.file;
                CommonService.success('文件上传完成');
            };
            uploader.onErrorItem = function () {
                CommonService.error('文件上传失败');
            };
            uploader.onCompleteAll = function () {
                uploader.clearQueue();
            };

            $scope.cancel = function () {
                uploader.cancelAll();
            };
        }
    }
});

app.directive('wysiwyg', function () {
    return {
        restrict: 'AE',
        scope: {
            ngHtml: '='
        },
        link: function (scope, elm, attrs, controller) {
            scope.ngHtml = function () {
                return $(elm).html();
            }
        }
    }
});

app.directive('product', function () {
    return {
        restrict: 'AE',
        scope: {
            product: '@'
        },
        template: "{{p.model}}",
        controller: function ($scope, $myhttp) {
            $scope.$watch('product', function () {
                $myhttp.get('/api/product/' + $scope.product).then(function (data) {
                    $scope.p = data;
                })
            });
        }
    }
});


app.directive('customer', function () {
    return {
        restrict: 'AE',
        scope: {
            customer: '@'
        },
        template: "{{c.name}}({{c.mobile}})",
        controller: function ($scope, $myhttp) {
            $scope.$watch('customer', function () {
                $myhttp.get('/api/customer/' + $scope.customer).then(function (data) {
                    $scope.c = data;
                })
            });
        }
    }
});

app.directive('terminal', function () {
    return {
        restrict: 'AE',
        scope: {
            terminal: '@'
        },
        template: "{{t.name}}",
        controller: function ($scope, $myhttp) {
            $scope.$watch('terminal', function () {
                if ($scope.terminal) {
                    $myhttp.get('/api/terminal/' + $scope.terminal).then(function (data) {
                        $scope.t = data;
                    })
                }
            });
        }
    }
});

function encodeParams(params) {
    var i = 0;
    var s = '';
    for (var key in params) {
        if (_.isObject(params[key])) {
            params[key] = JSON.stringify(params[key]);
        }
        if (i == 0) {
            s += '?';
        } else {
            s += '&';
        }
        s += key + "=" + params[key];
        i++;
    }
    return s;
}
