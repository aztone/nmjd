'use strict';

/* Controllers */

var app = angular.module('app.controllers', ['pascalprecht.translate', 'ngCookies']);
app.controller('AppCtrl', ['$scope', '$translate', '$localStorage', '$window', 'UserService', '$state', '$myhttp',
    function ($scope, $translate, $localStorage, $window, UserService, $state, $myhttp) {
        // add 'ie' classes to html
        var isIE = !!navigator.userAgent.match(/MSIE/i);
        isIE && angular.element($window.document.body).addClass('ie');
        isSmartDevice($window) && angular.element($window.document.body).addClass('smart');
        UserService.getUser().then(function (user) {
            $scope.user = user;
        });
        // config
        $scope.app = {
            name: '安宇易快互联专卖店',
            intro: '安宇易快互联专卖店',
            version: '0.0.1',
            // for chart colors
            color: {
                primary: '#7266ba',
                info: '#23b7e5',
                success: '#27c24c',
                warning: '#fad733',
                danger: '#f05050',
                light: '#e8eff0',
                dark: '#3a3f51',
                black: '#1c2b36'
            },
            settings: {
                themeID: 1,
                navbarHeaderColor: 'bg-black',
                navbarCollapseColor: 'bg-white-only',
                asideColor: 'bg-black',
                headerFixed: true,
                asideFixed: false,
                asideFolded: false
            }
        };

        $myhttp.get('/app').then(function (data) {
            $scope.app.name = data.name;
            $scope.app.intro = data.intro;
            $scope.app.simple_name = data.simple_name;
        });

        // save settings to local storage
        if (angular.isDefined($localStorage.settings)) {
            $scope.app.settings = $localStorage.settings;
        } else {
            $localStorage.settings = $scope.app.settings;
        }
        $scope.$watch('app.settings', function () {
            $localStorage.settings = $scope.app.settings;
        }, true);

        // angular translate
        $scope.lang = {isopen: false};
        $scope.langs = {zh_CN: '中文'};
        $scope.selectLang = $scope.langs[$translate.proposedLanguage()] || "中文";
        $scope.setLang = function (langKey, $event) {
            // set the current lang
            $scope.selectLang = $scope.langs[langKey];
            // You can change the language during runtime
            $translate.use(langKey);
            $scope.lang.isopen = !$scope.lang.isopen;
        };

        function isSmartDevice($window) {
            // Adapted from http://www.detectmobilebrowsers.com
            var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
            // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
            return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
        }

        UserService.getUser().then(function (user) {
            $scope.user = user;
        });
        $scope.has = function (id) {
            if ($scope.user)
                return UserService.has(id);
            else{
                UserService.getUser().then(function (user) {
                    $scope.user = user;
                    return UserService.has(id);
                });
            }

        }

    }
]);

// 登录控制器
app.controller('SigninController', ['$scope', '$myhttp', '$state', '$localStorage', 'UserService', function ($scope, $myhttp, $state, $localStorage, UserService) {
    $scope.uid = $localStorage.uid;
    $scope.passwd = $localStorage.passwd;

    $scope.login = function () {
        $scope.error = '';
        $myhttp.post('/access/login', {name: $scope.uid, passwd: $.md5($scope.passwd), dbid: 1}).then(function (result) {
            $localStorage.uid = $scope.uid;
            $localStorage.passwd = $scope.passwd;
            $localStorage.user = result;
            console.log("UserService.user",result);
            UserService.user = result;
            $state.go('app.dashboard')
        }, function (err) {
            $scope.error = err;
        })
    }
}]);

app.controller('MessageCtrl', ['$scope', '$rootScope', '$modal',
    function ($scope, $rootScope, $modal) {
        $scope.open = function (size, message) {
            $modal.open({
                templateUrl: 'views/blocks/message.html',
                controller: function ($scope, $timeout, $modalInstance) {
                    $scope.message = message;
                    $timeout(function () {
                        try {
                            $modalInstance.dismiss('close');
                        } catch (e) {
                        }
                    }, 2000)
                },
                size: size
            });
        };
        $rootScope.$on('message', function (e, message) {
            $scope.open('sm', message);
        })
    }
]);


/* web logic */
app.controller('UserCtrl', function ($scope, UserService) {
    $scope.user = UserService.user;
});

app.controller('SettingsCtrl', function ($scope, $state, $myhttp, UserService, CommonService, $log) {
    $scope.save = function () {
        if ($scope.newPasswd && $scope.newPasswd == $scope.confirmNewPasswd) {
            $log.info(UserService.user);
            $myhttp.put('/api/user/' + UserService.user._id, {
                user: {password: $.md5($scope.newPasswd)}
            }).then(function () {
                CommonService.success('修改成功');
                $state.go('access.signin')
            }, function (e) {
                CommonService.error(e.message);
            });
        } else {
            CommonService.error('两次输入的密码不正确');
        }
    }
});

app.controller('SmsSettingsCtrl', function ($scope, $myhttp, UserService, CommonService) {
    $scope.save = function () {
        if ($scope.sms.content) {
            $myhttp.put('/api/metadata/sms', {
                user: {content: $scope.sms.content}
            }).then(function () {
                CommonService.success('修改成功');
            }, function (e) {
                CommonService.error(e.message);
            });
        } else {
            CommonService.error('两次输入的密码不正确');
        }
    }
});