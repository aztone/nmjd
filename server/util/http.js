'use strict';
var URI = require('urijs');
var q = require('q');
var _ = require('lodash');
var request = require('request');
var logger = require('log4js').getLogger('http');
var fs = require('fs');
var path = require('path');

exports.get = function(url, params){
    return q.Promise(function(resolve, reject){
        var u = URI(url);
        if (params){
            _.mapValues(params, function(v, k){
                u.setQuery(k, v);
            })
        }

        logger.info('http get', u.href());
        request.get({url: u.href(), json: true}, function (err, res, body) {
            //logger.info('http return', JSON.stringify(body));
            if (err) {
                reject(new Error(err));
            } else {
                resolve(body);
            }
        });
    })
};

exports.getImg = function(url, params, filename){
    return q.Promise(function(resolve, reject){
        var u = URI(url);
        if (params){
            _.mapValues(params, function(v, k){
                u.setQuery(k, v);
            })
        }
        //logger.info('http get', u.href());
        var action = request(u.href());
        action.pipe(fs.createWriteStream(filename));
        action.on('end', function () {
            resolve(true);
        });
        action.on('error', function (err) {
            reject(new Error(err));
        });
    })
};


exports.post = function(url, params){
    return q.Promise(function(resolve, reject){
        request.post({
            url: url, form: JSON.stringify(params)
        }, function (err, res, body) {
            logger.info('http post return', JSON.stringify(body));
            if (err) {
                reject(new Error(err));
            } else {
                resolve(body);
            }
        })
    })
};
exports.postXml= function(url, params){
    return q.Promise(function(resolve, reject){
        request.post({
            url: url, form: params
        }, function (err, res, body) {
            logger.info('http post return', body);
            if (err) {
                reject(new Error(err));
            } else {
                resolve(body);
            }
        })
    })
};

exports.post1 = function(url, params, data){
    return q.Promise(function(resolve, reject){

        var u = URI(url);
        if (params){
            _.mapValues(params, function(v, k){
                u.setQuery(k, v);
            })
        }
        request.post({
            url: u.href(), form: JSON.stringify(data)
        }, function (err, res, body) {
            logger.info('http post return', body);
            if (err) {
                reject(new Error(err));
            } else {
                resolve(JSON.parse(body));
            }
        })
    })
};