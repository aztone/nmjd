/**
 * Created by aztone on 2015/4/6.
 */
'use strict';
var mongoose = require('./mongoose'), q = require('q'), Schema = require('./schema'), _ = require('lodash');

var logger = require('log4js').getLogger('dbl');
var save = mongoose.save, remove = mongoose.remove, update = mongoose.update;

var ERRORS = {POLICY_NOT_EXIST: '保修政策不存在'};

var Db = function (dbName) {
    this.dbName = 'wxt-policy-mxb_' + String(dbName);
};
module.exports = Db;

function pagination(count, curPage, limit) {
    limit = limit || 10;
    curPage = Number(curPage || 1);
    var r = {count: count};
    r.total = Math.floor(count / limit) + 1;
    if (curPage <= 0) {
        r.page = 1
    } else if (curPage > r.total) {
        r.page = r.total;
    } else {
        r.page = curPage;
    }
    return r;
}

function isJSON(v) {
    try {
        JSON.parse(v);
        return true;
    } catch (e) {
        return false;
    }
}

Db.prototype.objectId = function () {
    return mongoose.objectId();
};

Db.prototype.userFindOne = function (where) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var User = mongoose.model(this.dbName, Schema.user);
    return User.findOne(where).exec();
};

Db.prototype.userFind = function (where) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var User = mongoose.model(this.dbName, Schema.user);
    return User.find(where).exec();
};

Db.prototype.userFindByTerminal = function (_id) {
    var User = mongoose.model(this.dbName, Schema.user);
    return User.find({terminal: _id, state: {$ne: 'N'}}).exec();
};

Db.prototype.userAdd = function (terminal, data) {
    var User = mongoose.model(this.dbName, Schema.user);
    return User.findOne({name: data.name}).exec().then(function (doc) {
        if(doc){
            throw new Error('此账户已经存在');
        }else{
            var user = new User({
                name: data.name,
                password: data.password,
                features: data.features,
                terminal: terminal,
                state: 'Y',
                tel: data.tel
            });
            return mongoose.save(user);
        }
    })
};

Db.prototype.userModify = function (_id, data) {
    var User = mongoose.model(this.dbName, Schema.user);
    return User.findOne({_id: _id}).exec().then(function (user) {
        if (user) {
            user.name = data.name || user.name;
            user.password = data.password || user.password;
            user.features = data.features || user.features;
            user.terminal = data.terminal || user.terminal;
            user.tel = data.tel || user.tel;
            return mongoose.save(user);
        }
    })
};

Db.prototype.userFindAll = function () {
    var User = mongoose.model(this.dbName, Schema.user);
    return User.find().exec();
};

Db.prototype.userRemove = function (_id) {
    var User = mongoose.model(this.dbName, Schema.user);
    return User.remove({_id: _id}).exec();
};

Db.prototype.featureFind = function () {
    var Feature = mongoose.model(this.dbName, Schema.feature);
    return Feature.find().exec();
};

Db.prototype.customerFind = function (where, page, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var r;
    var Customer = mongoose.model(this.dbName, Schema.customer);
    return Customer.find(where).count().exec().then(function (count) {
        console.log(where);
        r = pagination(count, page, limit);
        return Customer.find(where).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    });
};

Db.prototype.customerFindAll = function () {
    var Customer = mongoose.model(this.dbName, Schema.customer);
    return Customer.find().exec();
};

Db.prototype.customerFindById = function (_id) {
    var Customer = mongoose.model(this.dbName, Schema.customer);
    return Customer.findOne({_id: _id}).exec();
};


Db.prototype.customerAdd = function (data) {
    var Customer = mongoose.model(this.dbName, Schema.customer);
    //return Customer.findOne({name: data.name, mobile: data.mobile}).exec().then(function (customer) {
    return Customer.findOne({mobile: data.mobile}).exec().then(function (customer) {
        if (customer) {
            customer.name = data.name || customer.name;
            customer.tel = data.tel || customer.tel;
            customer.mobile = data.mobile || customer.mobile;
            customer.email = data.email || customer.email;
            customer.sex = data.sex || customer.sex;
            customer.level = data.level || customer.level;
            customer.identity = data.identity || customer.identity;
            customer.company = data.company || customer.company;
            customer.province = data.province || customer.province;
            customer.city = data.city || customer.city;
            customer.area = data.area || customer.area;
            customer.address = data.address || customer.address;
            customer.update_at = new Date();
        } else {
            customer = new Customer({
                name: data.name,
                tel: data.tel,
                mobile: data.mobile,
                email: data.email,
                sex: data.sex,
                level: data.level,
                identity: data.identity,
                company: data.company,
                province: data.province,
                city: data.city,
                area: data.area,
                address: data.address,
                create_at: new Date
            });
        }
        return mongoose.save(customer);
    })
};

Db.prototype.customerModify = function (_id, data) {
    var Customer = mongoose.model(this.dbName, Schema.customer);
    return Customer.findOne({_id: _id}).exec().then(function (customer) {
        if (customer) {
            customer.name = data.name || customer.name;
            customer.tel = data.tel || customer.tel;
            customer.mobile = data.mobile || customer.mobile;
            customer.email = data.email || customer.email;
            customer.sex = data.sex || customer.sex;
            customer.level = data.level || customer.level;
            customer.identity = data.identity || customer.identity;
            customer.company = data.company || customer.company;
            customer.province = data.province || customer.province;
            customer.city = data.city || customer.city;
            customer.area = data.area || customer.area;
            customer.address = data.address || customer.address;
            customer.update_at = new Date();
            return mongoose.save(customer);
        }
    })
};

Db.prototype.customerRemove = function (_id) {
    var Customer = mongoose.model(this.dbName, Schema.customer);
    return Customer.remove({_id: _id}).exec();
};

Db.prototype.terminalAddRoot = function (data) {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    return Terminal.findOne({parent: null}).exec().then(function (terminal) {
        if (terminal) {
            return terminal;
        } else {
            terminal = new Terminal({
                name: data.name,
                ancestors: [],
                parent: null,
                tel: data.tel,
                province: data.province,
                city: data.city,
                area: data.area,
                address: data.address,
                state: 'Y',
                create_at: new Date()
            });
            return mongoose.save(terminal);
        }
    })
};

Db.prototype.terminalAdd = function (_parentId, data) {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    return Terminal.findOne({_id: _parentId}).exec().then(function (parent) {
        if (parent) {
            var ancestors = parent.ancestors.concat(parent._id);
            return Terminal.findOne({name: data.name}).exec().then(function (doc) {
                if (doc) {
                    throw new Error('此网点已经存在');
                } else {
                    var terminal = new Terminal({
                        name: data.name,
                        ancestors: ancestors,
                        parent: parent._id,
                        tel: data.tel,
                        province: data.province,
                        city: data.city,
                        area: data.area,
                        address: data.address,
                        state: 'Y',
                        dbid: data.dbid,
                        create_at: new Date()
                    });
                    return mongoose.save(terminal);
                }
            })
        }
    })
};


Db.prototype.terminalModify = function (_id, data) {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    var terminal;
    return q().then(function () {
        return Terminal.findOne({_id: _id}).exec()
    }).then(function (doc) {
        terminal = doc;
        if (terminal) {
            if (data.state == 'N') {
                return Terminal.update({
                    $or: [
                        {_id: _id},
                        {ancestors: {$all: [_id]}}
                    ]
                }, {state: 'N'}).exec();
            }
        } else {
            throw new Error('终端不存在');
        }
    }).then(function () {
        terminal.name = data.name || terminal.name;
        terminal.tel = data.tel || terminal.tel;
        terminal.province = data.province || terminal.province;
        terminal.city = data.city || terminal.city;
        terminal.area = data.area || terminal.area;
        terminal.address = data.address || terminal.address;
        terminal.state = data.state || terminal.state;
        terminal.dbid = data.dbid || terminal.dbid;
        return mongoose.save(terminal);
    })
};

Db.prototype.terminalFindAll = function () {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    return Terminal.find().exec();
};

Db.prototype.terminalFind = function () {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    return Terminal.find({state: {$ne: 'N'}}).exec();
};

Db.prototype.terminalFindById = function (_id) {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    return Terminal.findOne({_id: _id}).exec();
};

Db.prototype.terminalFindRoot = function () {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    return Terminal.find({parent: null}).exec();
};

Db.prototype.terminalFindSon = function (_id) {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    return Terminal.find({parent: _id, state: {$ne: 'N'}}).exec();
};

Db.prototype.terminalFindLeaf = function (_id, self) {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    if (self) {
        return Terminal.find({
            $or: [
                {ancestors: {$all: [_id]}},
                {_id: _id}
            ],
            state: {$ne: 'N'}
        }).exec();
    } else {
        if (self) {
            return Terminal.find({
                $or: [
                    {ancestors: {$all: [_id]}},
                    {_id: _id}
                ],
                state: {$ne: 'N'}
            }).exec();
        } else {
            return Terminal.find({ancestors: {$all: [_id]}}).exec();
        }
    }
};

Db.prototype.terminalFindPartLeaf = function (_id, key, self, limit) {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    if (self) {
        return Terminal.find({
            $or: [
                {ancestors: {$all: [_id]}},
                {_id: _id}
            ],
            name: {$regex: key},
            state: {$ne: 'N'}
        }).limit(limit).exec();
    } else {
        if (self) {
            return Terminal.find({
                $or: [
                    {ancestors: {$all: [_id]}},
                    {_id: _id}
                ],
                name: {$regex: key},
                state: {$ne: 'N'}
            }).limit(limit).exec();
        } else {
            return Terminal.find({ancestors: {$all: [_id]}, name: {$regex: key}}).limit(limit).exec();
        }
    }
};

Db.prototype.productFind = function (where, page, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var r;
    var Product = mongoose.model(this.dbName, Schema.product);
    return Product.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return Product.find(where).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    })
};

Db.prototype.productFindAll = function () {
    var Product = mongoose.model(this.dbName, Schema.product);
    return Product.find().exec();
};

Db.prototype.productFindById = function (_id) {
    var Product = mongoose.model(this.dbName, Schema.product);
    return Product.findOne({_id: _id}).exec();
};

Db.prototype.productAdd = function (data) {
    var Product = mongoose.model(this.dbName, Schema.product);
    return Product.findOne({model: data.model}).exec().then(function (product) {
        if (product) {
            product.name = data.name || product.name;
            product.model = data.model || product.model;
            product.brand = data.brand || product.brand;
            product.category = data.category || product.category;
            product.sub_category = data.sub_category || product.sub_category;
            product.type = data.type || product.type;
            product.update_at = new Date();
        } else {
            product = new Product({
                name: data.name,
                model: data.model,
                brand: data.brand,
                category: data.category,
                sub_category:data.sub_category,
                type: data.type,
                create_at: new Date()
            })
        }
        return mongoose.save(product);
    })
};

Db.prototype.productModify = function (_id, data) {
    var Product = mongoose.model(this.dbName, Schema.product);
    return Product.findOne({_id: _id}).exec().then(function (product) {
        if (product) {
            product.name = data.name || product.name;
            product.model = data.model || product.model;
            product.brand = data.brand || product.brand;
            product.category = data.category || product.category;
            product.sub_category = data.sub_category || product.sub_category;
            product.type = data.type || product.type;
            product.update_at = new Date();
            return mongoose.save(product);
        }
    })
};

Db.prototype.productRemove = function (_id) {
    var Product = mongoose.model(this.dbName, Schema.product);
    return Product.remove({_id: _id}).exec();
};

Db.prototype.policyFind = function (where, page, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var r;
    var Policy = mongoose.model(this.dbName, Schema.policy);
    return Policy.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return Policy.find(where).sort({create_at: -1}).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    })
};

Db.prototype.policyFindAll = function (where, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var Policy = mongoose.model(this.dbName, Schema.policy);
    return Policy.find(where).count().exec().then(function (count) {
        return Policy.find(where).sort({create_at: -1}).limit(limit).exec();
    })
};

Db.prototype.policyFindById = function (_id) {
    var Policy = mongoose.model(this.dbName, Schema.policy);
    return Policy.findOne({_id: _id}).exec();
};

Db.prototype.policyAdd = function (data) {
    var Policy = mongoose.model(this.dbName, Schema.policy);
    return Policy.findOne({sn: data.sn}).exec().then(function (doc) {
        var policy = new Policy({
            customer: data.customer,
            terminal: data.terminal,
            product: data.product,
            create_user: data.create_user,
            type: data.type,
            price: data.price,
            product_price: data.product_price,
            life: data.life,
            product_sn: data.product_sn,
            sn: data.sn,
            invoice: data.invoice,
            comment: data.comment,
            buy_at: data.buy_at,
            sell_at: data.sell_at,
            machine_begin_at: data.machine_begin_at,//延保整机开始日期
            keypart_begin_at: data.keypart_begin_at,//关键部位开始日期
            machine_end_at: data.machine_end_at,//延保整机结束日期
            keypart_end_at: data.keypart_end_at,//关键部位结束日期
            ori_begin_at: data.ori_begin_at,//发票购买日期
            ori_machine_end_at: data.ori_machine_end_at,//整机原保结束日期
            ori_keypart_end_at: data.ori_keypart_end_at,//关键部位原保结束日期
            create_at: new Date(),
            name: data.name,
            tel: data.tel,
            model: data.model,
            brand: data.brand,
            category: data.category,
            sub_category: data.sub_category,
            province: data.province,
            city: data.city,
            area: data.area,
            address: data.address,
            state: data.state,
            sale_name: data.sale_name,//销售员姓名
            sale_tel: data.sale_tel,//销售员手机号
            service_id: data.service_id, //服务单号

            out_trade_no: data.out_trade_no, //商户订单号
            bank_type:data.bank_type,//付款银行类型
            transaction_id:data.transaction_id,//订单号
            time_end:data.time_end,//支付完成时间

            machine_life:data.machine_life,//整机厂保年限
            key_part_life:data.key_part_life,//关键部位厂保年限

            pic_filename:data.pic_filename,//图片
            dbid:data.dbid
        });
        return mongoose.save(policy);
    })
};

Db.prototype.policyModify = function (_id, data) {
    logger.info(data);
    var Policy = mongoose.model(this.dbName, Schema.policy);
    return Policy.findOne({_id: _id}).exec().then(function (policy) {
        if (policy) {
            policy.customer = data.customer || policy.customer;
            policy.terminal = data.terminal || policy.terminal;
            policy.product = data.product || policy.product;
            policy.update_user = data.update_user || policy.update_user;
            policy.type = data.type || policy.type;
            policy.price = data.price || policy.price;
            policy.product_price = data.product_price || policy.product_price;
            policy.life = data.life || policy.life;
            policy.sn = data.sn || policy.sn;
            policy.product_sn = data.product_sn || policy.product_sn;
            policy.invoice = data.invoice || policy.invoice;
            policy.comment = data.comment || policy.comment;
            policy.buy_at = data.buy_at || policy.buy_at;
            policy.sell_at = data.sell_at || policy.sell_at;
            policy.machine_begin_at = data.machine_begin_at || policy.machine_begin_at;//延保整机开始日期
            policy.keypart_begin_at = data.keypart_begin_at || policy.keypart_begin_at;//关键部位开始日期
            policy.machine_end_at = data.machine_end_at || policy.machine_end_at;//延保整机结束日期
            policy.keypart_end_at = data.keypart_end_at || policy.keypart_end_at;//关键部位结束日期
            policy.ori_begin_at = data.ori_begin_at || policy.ori_begin_at;//发票购买日期
            policy.ori_machine_end_at = data.ori_machine_end_at || policy.ori_machine_end_at;//整机原保结束日期
            policy.ori_keypart_end_at = data.ori_keypart_end_at || policy.ori_keypart_end_at;//关键部位原保结束日期
            policy.update_at = new Date();

            policy.name = data.name || policy.name;
            policy.tel = data.tel || policy.tel;
            policy.model = data.name || policy.model;
            policy.brand = data.brand || policy.brand;
            policy.category = data.category || policy.category;
            policy.sub_category = data.sub_category || policy.sub_category;
            policy.province = data.tel || policy.province;
            policy.city = data.city || policy.city;
            policy.area = data.area || policy.area;
            policy.address = data.address || policy.address;
            policy.state = data.state || policy.state;

            policy.sale_name = data.sale_name || policy.sale_name;
            policy.sale_tel = data.sale_tel || policy.sale_tel;

            policy.out_trade_no = data.out_trade_no || policy.out_trade_no;//商户订单号
            policy.bank_type = data.bank_type || policy.bank_type;//付款银行类型
            policy.transaction_id = data.transaction_id || policy.transaction_id;//订单号
            policy.time_end = data.time_end || policy.transaction_id;//支付完成时间

            policy.machine_life = data.machine_life || policy.machine_life;//整机厂保年限
            policy.key_part_life = data.key_part_life || policy.key_part_life;//关键部位厂保年限

            policy.pic_filename = data.pic_filename || policy.pic_filename;//图片
            policy.dbid = data.dbid || policy.dbid;//图片
            return mongoose.save(policy);
        }
    })
};

Db.prototype.policyModifyBySn = function (data) {
    logger.info(data);
    var Policy = mongoose.model(this.dbName, Schema.policy);
    return Policy.findOne({sn: data.sn}).exec().then(function (policy) {
        if (policy) {
            policy.customer = data.customer || policy.customer;
            policy.terminal = data.terminal || policy.terminal;
            policy.product = data.product || policy.product;
            policy.update_user = data.update_user || policy.update_user;
            policy.type = data.type || policy.type;
            policy.price = data.price || policy.price;
            policy.product_price = data.product_price || policy.product_price;
            policy.life = data.life || policy.life;
            policy.sn = data.sn || policy.sn;
            policy.product_sn = data.product_sn || policy.product_sn;
            policy.invoice = data.invoice || policy.invoice;
            policy.comment = data.comment || policy.comment;
            policy.buy_at = data.buy_at || policy.buy_at;
            policy.sell_at = data.sell_at || policy.sell_at;
            policy.machine_begin_at = data.machine_begin_at || policy.machine_begin_at;//延保整机开始日期
            policy.keypart_begin_at = data.keypart_begin_at || policy.keypart_begin_at;//关键部位开始日期
            policy.machine_end_at = data.machine_end_at || policy.machine_end_at;//延保整机结束日期
            policy.keypart_end_at = data.keypart_end_at || policy.keypart_end_at;//关键部位结束日期
            policy.ori_begin_at = data.ori_begin_at || policy.ori_begin_at;//发票购买日期
            policy.ori_machine_end_at = data.ori_machine_end_at || policy.ori_machine_end_at;//整机原保结束日期
            policy.ori_keypart_end_at = data.ori_keypart_end_at || policy.ori_keypart_end_at;//关键部位原保结束日期
            policy.update_at = new Date();

            policy.name = data.name || policy.name;
            policy.tel = data.tel || policy.tel;
            policy.model = data.name || policy.model;
            policy.brand = data.brand || policy.brand;
            policy.category = data.category || policy.category;
            policy.sub_category = data.sub_category || policy.sub_category;
            policy.province = data.tel || policy.province;
            policy.city = data.city || policy.city;
            policy.area = data.area || policy.area;
            policy.address = data.address || policy.address;
            policy.state = data.state || policy.state;

            policy.sale_name = data.sale_name || policy.sale_name;
            policy.sale_tel = data.sale_tel || policy.sale_tel;

            policy.out_trade_no = data.out_trade_no || policy.out_trade_no;//商户订单号
            policy.bank_type = data.bank_type || policy.bank_type;//付款银行类型
            policy.transaction_id = data.transaction_id || policy.transaction_id;//订单号
            policy.time_end = data.time_end || policy.transaction_id;//支付完成时间

            policy.machine_life = data.machine_life || policy.machine_life;//整机厂保年限
            policy.key_part_life = data.key_part_life || policy.key_part_life;//关键部位厂保年限

            policy.pic_filename = data.pic_filename || policy.pic_filename;//图片
            policy.dbid = data.dbid || policy.dbid;//图片
            return mongoose.save(policy);
        }
    })
};

Db.prototype.policyRemove = function (_id) {
    var Customer = mongoose.model(this.dbName, Schema.policy);
    return Customer.remove({_id: _id}).exec();
};

Db.prototype.metadataListFind = function (where) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    return Metadata.find({state: 'Y'}).exec();
};

Db.prototype.metadataFind = function (where) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    return Metadata.find(where).exec();
};

Db.prototype.metadataFindNew = function (where) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    return Metadata.find({context: where}).exec();
};

Db.prototype.metadataFindAll = function (where) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    return Metadata.find(where).exec();
};

Db.prototype.metadataFindByKey = function (context, key) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    return Metadata.find({context: context , key: key}).exec();
};

Db.prototype.metadataRefFind = function (where) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    console.log(where);
    return Metadata.find({ref_context: where.ref_context ,ref_key:where.ref_key}).exec();
};

Db.prototype.metadataAdd = function (data) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    console.log(data);
    return q().then(function () {
        return Metadata.findOne({
            context: data.context,
            value: data.value
        }).exec();
    }).then(function (metadata) {
        if (!metadata) {
            metadata = new Metadata({
                context: data.context,
                key: data.key,
                value: data.value,
                remark: data.remark,
                state: 'Y',
                ref_context: data.ref_context,
                ref_key: data.ref_key
            });
            return mongoose.save(metadata);
        } else {
            return metadata;
        }
    });
};

Db.prototype.metadataModify = function (_id, data) {console.log("metadataModify",data)
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    var metadata;
    return Metadata.findOne({_id: _id}).exec().then(function (doc) {
        metadata = doc;
        return Metadata.find({context: data.context, value: data.value, _id: {$ne: _id}}).count().exec();
    }).then(function (count) {
        if (count > 0) {
            throw new Error('数据已存在');
        } else {
            if (!metadata) {
                metadata = new Metadata({
                    context: data.context,
                    key: data.key,
                    value: data.value,
                    remark: data.remark,
                    state: 'Y',
                    ref_context: data.ref_context,
                    ref_key: data.ref_key
                });
            } else {
                metadata.context = data.context || metadata.context;
                metadata.key = data.key || metadata.key;
                metadata.value = data.value || metadata.value;
                metadata.state = data.state || metadata.state;
                metadata.remark = data.remark || metadata.remark;
                metadata.ref_context = data.ref_context;
                metadata.ref_key = data.ref_key
            }
            return mongoose.save(metadata);
        }
    })
};

Db.prototype.metadataRemove = function (_id) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    return Metadata.remove({_id: _id}).exec();
};

Db.prototype.metadataRemoveByContextKey = function (context, key) {
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    return Metadata.remove({context: context, key: key}).exec();
};


Db.prototype.axbCardFind = function (where, page, limit) {
    if (where) {
        if (isJSON(where)) {
            where = JSON.parse(where);
        }
    } else {
        where = {};
    }
    var r = {};
    var AxbCard = mongoose.model(this.dbName, Schema.axbCard);
    return AxbCard.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return AxbCard.find(where).sort({
            create_at: -1
        }).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    });
};

Db.prototype.axbCardAdd = function (data) {
    var AxbCard = mongoose.model(this.dbName, Schema.axbCard);
    return q().then(function () {
        var axbCard = new AxbCard({
            code: data.code,
            create_at: new Date(),
            state: "0"
        });
        return mongoose.save(axbCard);
    })
};

Db.prototype.axbCardModify = function (_id, data) {
    var AxbCard = mongoose.model(this.dbName, Schema.axbCard);
    return AxbCard.findOne({_id: _id}).exec().then(function (axbCard) {
        if (axbCard) {
            axbCard.code = data.code || axbCard.code;
            axbCard.registe_at = data.registe_at || axbCard.registe_at;
            axbCard.state = data.state || axbCard.state;
            return mongoose.save(axbCard);
        }
    })
};

Db.prototype.axbFind = function (where, page, limit) {
    if (where) {
        if (isJSON(where)) {
            where = JSON.parse(where);
        }
    } else {
        where = {};
    }
    var r = {};
    var Axb = mongoose.model(this.dbName, Schema.axb);
    return Axb.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return Axb.find(where).sort({
            create_at: -1
        }).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    });
};

Db.prototype.axbAdd = function (data) {
    var Axb = mongoose.model(this.dbName, Schema.axb);
    return q().then(function () {
        var axb = new Axb({
            code: data.code,
            service_id: data.service_id,
            name: data.name,
            tel: data.tel,
            machine_type: data.machine_type,
            machine_code: data.machine_code,
            price: data.price,
            create_at: new Date(),
            state: data.state,
            com_region: data.com_region,
            com_province: data.com_province,
            com_city: data.com_city,
            category_id: data.category_id,
            policy_id: data.policy_id,
            time_fixed: data.time_fixed,
            fee: data.fee
        });
        return mongoose.save(axb);
    })
};

Db.prototype.axbModify = function (_id, data) {
    var Axb = mongoose.model(this.dbName, Schema.axb);
    return Axb.findOne({_id: _id}).exec().then(function (axb) {
        if (axb) {
            axb.code = data.code || axb.code;
            axb.service_id = data.service_id || axb.service_id;
            axb.tel = data.tel || axb.tel;
            axb.price = data.price || axb.price;
            axb.registe_at = data.registe_at || axb.registe_at;
            axb.state = data.state || axb.state;
            axb.com_region = data.com_region || axb.com_region;
            axb.com_province = data.com_province || axb.com_province;
            axb.com_city = data.com_city || axb.com_city;
            return mongoose.save(axb);
        }
    })
};


Db.prototype.brandCategoryFind = function (where) {
    var BrandCategory = mongoose.model(this.dbName, Schema.brandCategory);
    if (where) {
        where.state = 'Y';
        return BrandCategory.find(where, "brand_id category_id state").exec();
    } else {
        return BrandCategory.find({state: 'Y'}, "brand_id category_id state").exec();
    }
};

Db.prototype.brandCategoryFindOne = function (brandId, categoryId) {
    var BrandCategory = mongoose.model(this.dbName, Schema.brandCategory);
    return BrandCategory.findOne({brand_id: brandId, category_id: categoryId, state: 'Y'}).exec();
};

Db.prototype.brandCategoryModify = function (data) {
    var BrandCategory = mongoose.model(this.dbName, Schema.brandCategory);
    return BrandCategory.findOne({
        brand_id: data.brand_id,
        category_id: data.category_id
    }).exec().then(function (doc) {
        if (doc) {
            doc.state = data.state;
            doc.projects = data.projects || [];
            doc.properties = data.properties || [];
            return mongoose.save(doc);
        } else {
            if (data.state == 'Y') {
                doc = new BrandCategory({
                    brand_id: data.brand_id,
                    category_id: data.category_id,
                    state: data.state,
                    projects: data.projects || [],
                    properties: data.properties || [],
                    service_fee: []
                });
                return mongoose.save(doc);
            }
        }
    })
};

Db.prototype.versionFind = function () {
    var Version = mongoose.model(this.dbName, Schema.version);
    return Version.find().exec();
};

Db.prototype.versionInc = function (type) {
    var Version = mongoose.model(this.dbName, Schema.version);
    return Version.update({type: type}, {$inc: {version: 1}}, {upsert: true}).exec();
};


Db.prototype.policyConfigFind = function (where, page, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var r;
    var Policy = mongoose.model(this.dbName, Schema.policyConfig);
    return Policy.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return Policy.find(where).sort({create_at: -1}).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    })
};


Db.prototype.policyConfigAdd = function (data) {
    var Policy = mongoose.model(this.dbName, Schema.policyConfig);
    return q().then(function () {
        var policy = new Policy({
            policy_id: data.policy_id,
            category_id: data.category_id,
            time_fixed: data.time_fixed,
            lower: data.lower,
            upper: data.upper,
            fee: data.fee,
            create_at: new Date()
        });
        return mongoose.save(policy);
    })
};

Db.prototype.policyConfigModify = function (_id, policy) {
    var Policy = mongoose.model(this.dbName, Schema.policyConfig);
    return Policy.findOne({_id: _id}).then(function (doc) {
        if (doc) {
            doc.policy_id = policy.policy_id || doc.policy_id;
            doc.category_id = policy.category_id || doc.category_id;
            doc.time_fixed = policy.time_fixed || doc.time_fixed;
            doc.lower = policy.lower || doc.lower;
            doc.upper = policy.upper || doc.upper;
            doc.fee = policy.fee || doc.fee;
            doc.create_at = policy.create_at || doc.create_at;
        } else {
            throw new Error(ERRORS.POLICY_NOT_EXIST);
        }
        return mongoose.save(doc);
    })
};

Db.prototype.policyConfigRemove = function (data) {
    var ServiceFee = mongoose.model(this.dbName, Schema.policyConfig);
    return ServiceFee.remove({_id: data}).exec();
};

Db.prototype.smsModify = function (sms) {
    var Sms = mongoose.model(this.dbName, Schema.sms);
    return Sms.findOne({type: 'sms'}).then(function (doc) {
        if (doc) {
            doc.content = sms.content || sms.content;
        } else {
            throw new Error(ERRORS.POLICY_NOT_EXIST);
        }
        return mongoose.save(doc);
    })
};

Db.prototype.mxbCardFind = function (where, page, limit) {
    if (where) {
        if (isJSON(where)) {
            where = JSON.parse(where);
        }
    } else {
        where = {};
    }
    var r = {};
    var MxbCard = mongoose.model(this.dbName, Schema.mxbCard);
    return MxbCard.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return MxbCard.find(where).sort({
            create_at: -1
        }).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    });
};

Db.prototype.mxbCardAdd = function (data) {
    var MxbCard = mongoose.model(this.dbName, Schema.mxbCard);
    return q().then(function () {
        var mxbCard = new MxbCard({
            code: data.code,
            create_at: new Date(),
            state: "0"
        });
        return mongoose.save(mxbCard);
    })
};

//Db.prototype.mxbCardImport = function (data) {
//    var MxbCard = mongoose.model(this.dbName, Schema.mxbCard);
//    return q().then(function () {
//        var mxbCard = new MxbCard({
//            code: data.code,
//            create_at: data.create_at,
//            state: data.state,
//            registe_at: data.registe_at
//        });
//        return mongoose.save(mxbCard);
//    })
//};

Db.prototype.mxbCardImport = function (data) {
    var MxbCard = mongoose.model(this.dbName, Schema.mxbCard);
    var Metadata = mongoose.model(this.dbName, Schema.metadata);
    var typeId, categoryId, subCategoryId,lifeId;
    return Metadata.findOne({context: 'product.category', value: data.category}).exec().then(function (doc) {
        if (doc) {
            categoryId = doc.key;
            return Metadata.findOne({context: 'product.sub_category', value: data.sub_category}).exec().then(function (doc) {
                if (doc) {
                    subCategoryId = doc.key;
                    return Metadata.findOne({context: 'policy.type', value: data.type}).exec().then(function (doc) {
                        if (doc) {
                            typeId = doc.key;
                            return Metadata.findOne({context: 'policy.life', value: data.life}).exec().then(function (doc) {
                                if (doc) {
                                    lifeId = doc.key;
                                    var mxbCard = new MxbCard({
                                        code: data.code,
                                        create_at: data.create_at,
                                        state: data.state,
                                        registe_at: data.registe_at,
                                        type: typeId,
                                        category: categoryId,
                                        sub_category: subCategoryId,
                                        price: data.price,
                                        life: lifeId
                                    });
                                    return mongoose.save(mxbCard).then(function (doc) {
                                        return 0;
                                    });
                                } else {
                                    return 1
                                }
                            })
                        } else {
                            logger.info("%s 已存在", JSON.stringify({name: item.name, model: item.model, intro: item.intro, code: item.code}));
                            return 2;
                        }
                    })
                } else {
                    return 1;
                }
            })
        } else {
            return 1;
        }
    })
};

Db.prototype.mxbCardModify = function (_id, data) {
    var MxbCard = mongoose.model(this.dbName, Schema.mxbCard);
    return MxbCard.findOne({_id: _id}).exec().then(function (mxbCard) {
        if (mxbCard) {
            mxbCard.code = data.code || mxbCard.code;
            mxbCard.registe_at = data.registe_at || mxbCard.registe_at;
            mxbCard.state = data.state || mxbCard.state;
            mxbCard.life = data.state || mxbCard.state;
            mxbCard.category = data.category || mxbCard.category;
            mxbCard.sub_category = data.sub_category || mxbCard.sub_category;
            mxbCard.tpye = data.tpye || mxbCard.tpye;
            mxbCard.price = data.price || mxbCard.price;
            mxbCard.create_at = data.create_at || mxbCard.create_at;
            return mongoose.save(mxbCard);
        }
    })
};

Db.prototype.interfacePolicyAdd = function (data) {
    var InterfacePolicy = mongoose.model(this.dbName, Schema.interfacePolicy);
    return InterfacePolicy.findOne({sn: data.sn }).exec().then(function (interfacePolicy) {
        if (interfacePolicy) {
            //throw new Error('此订单已经存在');
            interfacePolicy.state = 'Y' || interfacePolicy.state;
            interfacePolicy.type = data.type || interfacePolicy.type;
            interfacePolicy.sn = data.sn || interfacePolicy.sn;
            interfacePolicy.name = data.name || interfacePolicy.name;
            interfacePolicy.tel = data.tel || interfacePolicy.tel;
            interfacePolicy.address = data.address || interfacePolicy.address;

            interfacePolicy.insureInfo = data.insureInfo || interfacePolicy.insureInfo;

            interfacePolicy.pay_at = data.pay_at || interfacePolicy.pay_at;
            interfacePolicy.registe_at = data.registe_at || interfacePolicy.registe_at;
            interfacePolicy.updata_at = new Date();
            interfacePolicy.price = data.price || interfacePolicy.price;
        } else {
            var interfacePolicy = new InterfacePolicy({
                state: 'Y',//Y有效
                type: data.type,
                sn: data.sn, //对应axb的code    订单号
                name:data.name,//用户姓名
                tel:data.tel, //联系方式
                address:data.address,//地址

                insureInfo:data.insureInfo,//投保信息数组

                pay_at:data.pay_at,//完成支付时间
                registe_at:new Date(),
                price:data.price
            });
        }
        return mongoose.save(interfacePolicy);
    })
};

Db.prototype.interfacePolicyCount = function (data) {
    var InterfacePolicy = mongoose.model(this.dbName, Schema.interfacePolicy);
    var Policy = mongoose.model(this.dbName, Schema.policy);
    var count;
    return Policy.findOne({tel: data.tel }).count().exec().then(function (count_policy) {
        count = count_policy;
        return InterfacePolicy.findOne({tel: data.tel}).count().exec().then(function (count_interfacePolicy) {
            count = count_interfacePolicy + count_policy + 1;
            if (count < 10) {
                count = data.tel + "00" + count;
            } else if (count < 100) {
                count = data.tel + "0" + count;
            } else if (count < 1000) {
                count = data.tel + "" + count;
            }
            data.service_id = count;
            return data;
        })
    })
};

Db.prototype.interfacePolicyFind = function (where, page, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var r;
    var InterfacePolicy = mongoose.model(this.dbName, Schema.interfacePolicy);
    return InterfacePolicy.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return InterfacePolicy.find(where).sort({registe_at: -1}).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        logger.info(r);
        return r;
    })
};

Db.prototype.interfacePolicyFindBySn = function (sn) {
    var InterfacePolicy = mongoose.model(this.dbName, Schema.interfacePolicy);
    return InterfacePolicy.findOne({sn: sn}).exec();
};

Db.prototype.interfaceServiceAdd = function (data) {
    //logger.info(data);
    var InterfaceService = mongoose.model(this.dbName, Schema.interfaceService);
    return InterfaceService.findOne({service_id: data.service_id }).exec().then(function (interfaceService) {
        if (interfaceService) {
            //throw new Error('此订单已经存在');
            interfaceService.state = 'Y' || interfaceService.state;
            interfaceService.type = data.type || interfaceService.type;
            interfaceService.sn = data.sn || interfaceService.sn;
            interfaceService.service_id = data.service_id || interfaceService.service_id;
            interfaceService.name = data.name || interfaceService.name;
            interfaceService.tel = data.tel || interfaceService.tel;
            interfaceService.address = data.address || interfaceService.address;
            interfaceService.serviceType = data.serviceType || interfaceService.serviceType;

            interfaceService.category = data.category || interfaceService.category;
            interfaceService.brand = data.brand || interfaceService.brand;
            interfaceService.model = data.model || interfaceService.model;
            interfaceService.product_sn = data.product_sn || interfaceService.product_sn;
            interfaceService.product_price = data.product_price || interfaceService.product_price;
            interfaceService.buy_at = data.buy_at || interfaceService.buy_at;
            interfaceService.receipt_pic = data.receipt_pic || interfaceService.receipt_pic;
            interfaceService.policy_machine = data.policy_machine || interfaceService.policy_machine;
            interfaceService.policy_keypart = data.policy_keypart || interfaceService.policy_keypart;
            interfaceService.life = data.life || interfaceService.life;
            interfaceService.pay_at = data.pay_at || interfaceService.pay_at;
            interfaceService.registe_at = data.registe_at || interfaceService.registe_at;
            interfaceService.singlePrice  = data.singlePrice  || interfaceService.singlePrice ;
            interfaceService.updata_at = new Date();
        } else {
            var interfaceService = new InterfaceService({
                state: 'Y',//Y有效
                type: data.type,
                sn: data.sn, //对应axb的code    订单号
                service_id: data.service_id,
                name:data.name,//用户姓名
                tel:data.tel, //联系方式
                address:data.address,//地址
                serviceType: data.serviceType,//延保类型
                category: data.category,//延保品类
                brand: data.brand,//延保品牌
                model:data.model, //机器型号
                product_sn: data.product_sn,//机器序列，机身码
                product_price: data.product_price,//发票价格
                buy_at: data.buy_at,//发票日期
                //receipt_pic: [String],
                receipt_pic: data.receipt_pic,
                policy_machine : data.policy_machine,//厂保政策
                policy_keypart : data.policy_keypart,//厂保政策
                life: data.life,//延保时长

                pay_at:data.pay_at,//完成支付时间
                registe_at :new Date(),
                singlePrice:data.singlePrice
            });
        }
        return mongoose.save(interfaceService);
    })
};

Db.prototype.interfaceServiceModify = function (data) {
    //logger.info(data);
    var InterfaceService = mongoose.model(this.dbName, Schema.interfaceService);
    return InterfaceService.findOne({service_id: data.service_id }).exec().then(function (interfaceService) {
        if (interfaceService) {
            //throw new Error('此订单已经存在');
            interfaceService.state = 'Y' || interfaceService.state;
            interfaceService.type = data.type || interfaceService.type;
            interfaceService.sn = data.sn || interfaceService.sn;
            interfaceService.service_id = data.service_id || interfaceService.service_id;
            interfaceService.name = data.name || interfaceService.name;
            interfaceService.tel = data.tel || interfaceService.tel;
            interfaceService.address = data.address || interfaceService.address;
            interfaceService.serviceType = data.serviceType || interfaceService.serviceType;

            interfaceService.category = data.category || interfaceService.category;
            interfaceService.brand = data.brand || interfaceService.brand;
            interfaceService.model = data.model || interfaceService.model;
            interfaceService.product_sn = data.product_sn || interfaceService.product_sn;
            interfaceService.product_price = data.product_price || interfaceService.product_price;
            interfaceService.buy_at = data.buy_at || interfaceService.buy_at;
            interfaceService.receipt_pic = data.receipt_pic || interfaceService.receipt_pic;
            interfaceService.policy_machine = data.policy_machine || interfaceService.policy_machine;
            interfaceService.policy_keypart = data.policy_keypart || interfaceService.policy_keypart;
            interfaceService.life = data.life || interfaceService.life;
            interfaceService.pay_at = data.pay_at || interfaceService.pay_at;
            interfaceService.registe_at = data.registe_at || interfaceService.registe_at;
            interfaceService.singlePrice  = data.singlePrice  || interfaceService.singlePrice ;
            interfaceService.updata_at = new Date();
        } else {
            var interfaceService = new InterfaceService({
                state: 'Y',//Y有效
                type: data.type,
                sn: data.sn, //对应axb的code    订单号
                service_id: data.service_id,
                name:data.name,//用户姓名
                tel:data.tel, //联系方式
                address:data.address,//地址
                serviceType: data.serviceType,//延保类型
                category: data.category,//延保品类
                brand: data.brand,//延保品牌
                model:data.model, //机器型号
                product_sn: data.product_sn,//机器序列，机身码
                product_price: data.product_price,//发票价格
                buy_at: data.buy_at,//发票日期
                //receipt_pic: [String],
                receipt_pic: data.receipt_pic,
                policy_machine : data.policy_machine,//厂保政策
                policy_keypart : data.policy_keypart,//厂保政策
                life: data.life,//延保时长

                pay_at:data.pay_at,//完成支付时间
                registe_at :new Date(),
                singlePrice:data.singlePrice
            });
        }
        return mongoose.save(interfaceService);
    })
};

Db.prototype.interfaceServiceCount = function (data) {
    var InterfaceService = mongoose.model(this.dbName, Schema.interfaceService);
    var Policy = mongoose.model(this.dbName, Schema.policy);
    var count;
    return Policy.findOne({tel: data.tel }).count().exec().then(function (count_policy) {
        count = count_policy;
        return InterfaceService.findOne({tel: data.tel, serviceType: '1'}).count().exec().then(function (count_interfaceService) {
            count = count_interfaceService + count_policy + 1;
            /*if (count < 10) {
             count = data.tel + "00" + count;
             } else if (count < 100) {
             count = data.tel + "0" + count;
             } else if (count < 1000) {
             count = data.tel + "" + count;
             }*/
            return count;
        })
    })
};

Db.prototype.interfaceServiceQuanjiaCount = function (data) {
    var InterfaceService = mongoose.model(this.dbName, Schema.interfaceService);
    var count;

    return InterfaceService.findOne({tel: data.tel, serviceType: '2'}).count().exec().then(function (count_interfaceService) {
        count = count_interfaceService + 1;
        /*if (count < 10) {
         count = "QJB_" + data.tel + "00" + count;
         } else if (count < 100) {
         count = "QJB_" + data.tel + "0" + count;
         } else if (count < 1000) {
         count = "QJB_" + data.tel + "" + count;
         }*/
        return count;
    })
};

Db.prototype.interfaceServiceFind = function (where, page, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var r;
    var InterfaceService = mongoose.model(this.dbName, Schema.interfaceService);
    return InterfaceService.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return InterfaceService.find(where).sort({registe_at: -1}).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        logger.info(r);
        return r;
    })
};

Db.prototype.userImport = function (data) {
    var Terminal = mongoose.model(this.dbName, Schema.terminal);
    var User = mongoose.model(this.dbName, Schema.user);
    var terminalId;
    return Terminal.findOne({name: data.terminal}).exec().then(function (doc) {
        if (doc) {
            terminalId = doc._id;
            return User.findOne({name: data.name}).exec().then(function (user) {
                if(user){
                    user.name = data.name || user.name;
                    user.password = data.password || user.password;
                    user.terminal = terminalId || user.terminal;
                    user.features = [3,4,5];
                    user.state = "Y";
                    user.tel = data.tel || user.tel;
                }else{
                    var user = new User({
                        name: data.name,
                        password:data.password,
                        terminal: terminalId,
                        features: [3,4,5],
                        state: "Y",
                        tel: data.tel
                    });
                }
                return mongoose.save(user).then(function (doc) {
                    return 0;
                });
            });
        } else {
            //logger.info("%s 已存在", JSON.stringify({name: item.name, model: item.model, intro: item.intro, code: item.code}));
            return 1;
        }
    })
};

Db.prototype.QxjCardFind = function (where, page, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var r;
    var CleanCard = mongoose.model(this.dbName, Schema.cleanCard);
    return CleanCard.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return CleanCard.find(where).sort({_id: -1}).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    })
};

Db.prototype.QxjCardImport = function (data) {
    logger.info(data);
    var CleanCard = mongoose.model(this.dbName, Schema.cleanCard);
    return CleanCard.findOne({code: data.code}).exec().then(function (card) {
        if (!card) {
            logger.info('1');
            var card = new CleanCard({
                code: data.code,
                state: "0",//清洗卡状态
                cardType: data.cardType,//清洗卡类型，A 家电深度清洁；B 汽车空调深度清洁
                deductible_amount:data.deductible_amount,//清洁卡抵扣的金额
                create_at: data.create_at ||  new Date()//建单时间
            });
        }else{
            logger.info('2');
            card.code = data.code || card.code;
            card.cardType = data.cardType || card.cardType;
            card.deductible_amount = data.deductible_amount || card.deductible_amount;
            card.create_at = data.create_at || card.create_at;
            card.update_at = new Date();
        }

        logger.info(card);
        return mongoose.save(card).then(function (doc) {
            return 0;
        });
    })
};

Db.prototype.QxjCardFindByCode = function (code) {
    var CleanCard = mongoose.model(this.dbName, Schema.cleanCard);
    return CleanCard.findOne({code: code}).exec();
};

Db.prototype.cleanSheetFind = function (where, page, limit) {
    if (isJSON(where)) {
        where = JSON.parse(where);
    }
    var r;
    var Sheet = mongoose.model(this.dbName, Schema.sheet);
    return Sheet.find(where).count().exec().then(function (count) {
        r = pagination(count, page, limit);
        return Sheet.find(where).sort({_id: -1}).skip((r.page - 1) * limit).limit(limit).exec();
    }).then(function (docs) {
        r.data = docs;
        return r;
    })
};


Db.prototype.cleanSheetAdd = function (data) {
    logger.info(data);
    var Sheet = mongoose.model(this.dbName, Schema.sheet);
    return q().then(function () {
        var sheet = new Sheet({
            dispatch_state:'0',//派单状态,0 未派单；1 已派单
            state: data.state,//清洗单的支付状态，0 未支付；1 已支付
            sheetType: data.sheetType,//清洗单类型，A 家电深度清洁；B 汽车空调深度清洁
            number:data.number,//购买的份数
            claeanCard:data.claeanCard,//清洁卡号
            deductible_amount:data.deductible_amount,//清洁卡抵扣的金额
            total_amount:data.total_amount,//总金额
            price: data.price,//待支付金额
            car_model:data.car_model,//车型号
            name:data.name,//用户姓名
            tel:data.tel, //联系方式
            province: data.province,//省份
            city: data.city,//地市
            area: data.area, //区县
            address:data.address,//地址

            service_at:data.service_at,//服务时间
            create_at: new Date()//建单时间
        });
        return mongoose.save(sheet);
    })
};

Db.prototype.cleanSheetModify = function (_id, data) {
    logger.info(data);
    var Sheet = mongoose.model(this.dbName, Schema.sheet);
    return Sheet.findOne({_id: _id}).exec().then(function (policy) {
        if (policy) {
            policy.dispatch_user = data.dispatch_user || policy.dispatch_user;
            policy.dispatch_state = data.dispatch_state || policy.dispatch_state;
            policy.state = data.state || policy.state;
            policy.sheetType = data.sheetType || policy.sheetType;
            policy.number = data.number || policy.number;
            policy.claeanCard = data.claeanCard || policy.claeanCard;
            policy.deductible_amount = data.deductible_amount || policy.deductible_amount;
            policy.total_amount = data.total_amount || policy.total_amount;
            policy.price = data.price || policy.price;
            policy.car_model = data.car_model || policy.car_model;
            policy.name = data.name || policy.name;
            policy.tel = data.tel || policy.tel;
            policy.address = data.address || policy.address;
            policy.service_at = data.service_at || policy.service_at;
            policy.create_at = data.create_at || policy.create_at;
            policy.pay_at = data.pay_at || policy.pay_at;//延保整机开始日期

            policy.bank_type = data.bank_type || policy.bank_type;
            policy.transaction_id = data.transaction_id || policy.transaction_id;
            policy.time_end = data.time_end || policy.time_end;

            policy.update_at = new Date();

            return mongoose.save(policy);
        }
    })
};