/**
 * Created by aztone on 2015/4/6.
 */
'use strict';

var Schema = require('mongoose').Schema, moment = require('moment');
Date.prototype.toJSON = function () {
    return moment(this).format('YYYY-MM-DD');
};

Date.prototype.toString = function () {
    return moment(this).format('YYYY-MM-DD');
};

exports.metadata = {
    model: 'Metadata',
    schema: new Schema({
        context: String,
        key: String,
        value: String,
        state: String,
        remark: String,
        ref_context:String,
        ref_key:String
    })
};

exports.user = {
    model: 'User',
    schema: new Schema({
        name: {type: String, unique: true, required: true},
        password: {type: String},
        terminal: {type: Schema.Types.ObjectId},
        features: [Number],
        state: String,
        tel:String
    })
};

exports.feature = {
    model: 'Feature',
    schema: new Schema({
        id: Number,
        name: String
    })
};

exports.terminal = {
    model: 'Terminal',
    schema: new Schema({
        name: {type: String, unique: true, require: true},
        ancestors: [Schema.Types.ObjectId],
        parent: Schema.Types.ObjectId,
        tel: String,
        province: Number,
        city: Number,
        area: Number,
        address: String,
        state: String,
        dbid:String,
        create_at: Date,
        update_at: Date
    })
};

exports.customer = {
    model: 'Customer',
    schema: new Schema({
        name: {type: String, required: true},
        state: String,
        tel: String,
        mobile: String,
        email: String,
        sex: String,
        level: String,
        identity: String,
        company: String,
        province: Number,
        city: Number,
        area: Number,
        address: String,
        create_at: Date,
        update_at: Date
    })
};

exports.policy = {
    model: 'Policy',
    schema: new Schema({
        customer: {type: Schema.Types.ObjectId, ref: 'Customer'},
        terminal: {type: Schema.Types.ObjectId, ref: 'Terminal'},
        product: {type: Schema.Types.ObjectId, ref: 'Product'},
        create_user: {type: Schema.Types.ObjectId, ref: 'User'},
        update_user: {type: Schema.Types.ObjectId, ref: 'User'},
        state: String,//Y有效
        type: String,//延保政策
        product_price: Number,//商品价格
        price: Number,//延保费用
        life: String,//延保年限
        product_sn: String,//机器编码
        sn: String, //对应axb的code
        invoice: String,
        comment: String,
        buy_at: Date,
        sell_at: Date,
        machine_begin_at: Date,//延保整机开始日期
        keypart_begin_at: Date,//关键部位开始日期
        machine_end_at: Date,//延保整机结束日期
        keypart_end_at: Date,//关键部位结束日期
        ori_begin_at: Date,//发票购买日期
        ori_machine_end_at: Date,//整机原保结束日期
        ori_keypart_end_at: Date,//关键部位原保结束日期
        create_at: Date,
        update_at: Date,

        name:String,//用户姓名
        tel:String, //联系方式
        model:String, //机器型号
        brand: String,//延保品牌
        category: String,//延保品类
        sub_category:String,
        province: Number,//省份
        city: Number,//地市
        area: Number, //区县
        address:String,//地址

        sale_name:String,//销售员姓名
        sale_tel:String,//销售员手机号

        out_trade_no:String, //商户订单号
        bank_type:String,//付款银行类型
        transaction_id:String,//订单号
        time_end:String,//支付完成时间

        machine_life:String,//整机厂保年限
        key_part_life:String,//关键部位厂保年限

        //pic_filename:String//图片
        pic_filename: [
            {localId:String, serverId:String, fileName: String, ngUrl:String}
        ],
        dbid: String


    })
};

exports.interfacePolicy = {
    model: 'InterfacePolicy',
    schema: new Schema({
        state: String,//Y有效
        type: String,
        sn: String, //对应axb的code    订单号
        name:String,//用户姓名
        tel:String, //联系方式
        address:String,//地址
        insureInfo: [
            {
                serviceType: String,//延保类型
                category: String,//延保品类
                brand: String,//延保品牌
                model:String, //机器型号
                product_sn: String,//机器序列，机身码
                product_price: Number,//发票价格
                buy_at: Date,//发票日期
                //receipt_pic: [String],
                receipt_pic: [{url:String, localurl:String}],
                policy_machine : String,//厂保政策
                policy_keypart : String,//厂保政策
                life: String,//延保时长
                singlePrice :Number,
                service_id: String
            }
        ],
        pay_at:Date,//完成支付时间,
        registe_at:Date,
        updata_at:Date,
        price:Number
    })
};

exports.interfaceService = {
    model: 'InterfaceService',
    schema: new Schema({
        state: String,//Y有效
        type: String,
        sn: String, //对应axb的code    订单号
        service_id: String,
        name:String,//用户姓名
        tel:String, //联系方式
        address:String,//地址
        serviceType: String,//延保类型
        category: String,//延保品类
        brand: String,//延保品牌
        model:String, //机器型号
        product_sn: String,//机器序列，机身码
        product_price: Number,//发票价格
        buy_at: Date,//发票日期
        //receipt_pic: [String],
        receipt_pic: [{url:String, localurl:String}],
        policy_machine : String,//厂保政策
        policy_keypart : String,//厂保政策
        life: String,//延保时长
        pay_at:Date,//完成支付时间,
        registe_at:Date,
        updata_at:Date,
        singlePrice :Number
    })
};

exports.product = {
    model: 'Product',
    schema: new Schema({
        name: String,
        model: String,
        brand: String,
        category: String,
        sub_category:String,
        type: String,
        create_at: Date,
        update_at: Date
    })
};

exports.axb = {
    model: 'Axb',
    schema: new Schema({
        code: String,
        service_id: String,
        name: String,//用户姓名
        tel: String,//联系方式
        machine_type: String,//机器型号
        machine_code: String,//机器编码
        price: String,//商品价格
        state: String,//支付状态，Y支付，N未支付
        create_at: Date,
        registe_at: Date,
        com_region: String,
        com_province: String,
        com_city: String,
        category_id: String,//延保品类
        policy_id: String,//延保政策
        time_fixed: String,//延保年限
        fee: String//延保费用
    })
};
exports.axbCard = {
    model: 'AxbCard',
    schema: new Schema({
        code: String,
        state: String,//0有效 1无效
        create_at: Date,
        registe_at: Date
    })
};

exports.mxbCard = {
    model: 'MxbCard',
    schema: new Schema({
        code: String,
        state:String,//0有效 1无效
        create_at:Date,
        registe_at:Date,
        category:String,
        sub_category:String,
        type:String,
        life:String,
        price:String
    })
};

//品牌品类对应表
exports.brandCategory = {
    model: 'BrandCategory',
    schema: new Schema({
        brand_id: {type: Schema.Types.ObjectId, required: true},
        category_id: {type: Schema.Types.ObjectId, required: true},
        state: String,
        properties: [String],
        projects: [String],
        sync_id: Schema.Types.ObjectId
    })
};
//延保参数
exports.policyConfig = {
    model: 'PolicyConfig',
    schema: new Schema({
        policy_id: String,//延保描述
        category_id: String,//品类
        time_fixed: String,
        lower: Number,
        upper: Number,
        create_at: Date,
        fee: Number
    })
};
//版本号
exports.version = {
    model: 'Version',
    schema: new Schema({
        type: String,
        version: {type: Number, default: 0}
    })
};

//短信模板
exports.sms = {
    model: 'sms',
    schema: new Schema({
        type: String,
        content: String
    })
};

//清洗单
exports.sheet = {
    model: 'Sheet',
    schema: new Schema({
        dispatch_user: {type: Schema.Types.ObjectId, ref: 'User'},//派单的员工
        dispatch_state:String,//派单状态
        state: String,//清洗单的支付状态，N 未支付；Y 已支付
        sheetType: String,//清洗单类型，A 家电深度清洁；B 汽车空调深度清洁
        number:Number,//购买的份数
        claeanCard:String,//清洁卡号
        deductible_amount:Number,//清洁卡抵扣的金额
        total_amount:Number,//总金额
        price: Number,//待支付金额
        car_model:String,//车型号
        name:String,//用户姓名
        tel:String, //联系方式
        province: Number,//省份
        city: Number,//地市
        area: Number, //区县
        address:String,//地址

        bank_type: String,//付款银行类型
        transaction_id: String,//订单号
        time_end: String,//支付完成时间


        service_at:Date,//服务时间
        create_at: Date,//建单时间
        pay_at:Date,//支付时间
        update_at: Date//更新时间
    })
};

//清洗卡
exports.cleanCard = {
    model: 'CleanCard',
    schema: new Schema({
        cleanSheet: {type: Schema.Types.ObjectId},//对应的清洗单
        state: String,//清洗卡状态
        cardType: String,//清洗卡类型，A 家电深度清洁；B 汽车空调深度清洁
        code:String,//清洁卡号
        deductible_amount:Number,//清洁卡抵扣的金额
        create_at: Date,//建单时间
        update_at: Date//更新时间
    })
};