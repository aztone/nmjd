/**
 * Created by aztone on 2015/4/6.
 */
'use strict';
var mongoose = require('mongoose'), q = require('q'), conf = require('../config/db.json'), _ = require('lodash'), Schema = require('./schema');
var logger = require('log4js').getLogger('mongoose');
var base = mongoose.connect(conf.url, function (err) {
    if (err) {
        throw new Error('数据库连接异常');
    }
});


var pool = [];

exports.model = function(dbName, schema){
    var db = _.find(pool, {name: dbName});
    if (!db){
        var dbConn = base.connection.useDb(dbName);
        db = {name:dbName, db: dbConn};
        pool.push(db);
    }
    return db.db.model(schema.model, schema.schema);        
};

exports.objectId = function () {
    return mongoose.Types.ObjectId();
};

exports.save = function(doc){
    logger.info('数据准备保存:%s', JSON.stringify(doc));
    return q.Promise(function(resolve, reject){
        doc.save(function(err, doc){
            if (err){
                logger.warn('数据保存失败:%s,%s', JSON.stringify(doc), err.stack);
                reject(err);                
            }else{
                logger.info('数据保存成功:%s', JSON.stringify(doc));
                resolve(doc);
            }
        })   
    })
};

exports.update = function(Model, where, set){
    return Model.update(where, set, {upsert: true}).exec();
};

exports.remove = function(Model, where){
    return Model.remove(where).exec();
};

exports.seq = function(dbName, modelName){
    return q.Promise(function(resolve, reject){
        var Model = exports.model(dbName, 'Seq', Schema.seq);
        Model.findOneAndUpdate({name: modelName}, {$inc: {id:1}}, {upsert: true}, function(err, doc){
            if (err){
                reject(err);
            }else{
                if (doc){
                    resolve(doc.id);
                }else{
                    resolve(1);
                }                
            }            
        })
    })    
};



