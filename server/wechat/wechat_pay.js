'use strict';
var q = require('q'), _ = require('lodash');
var logger = require('log4js').getLogger('wechat_pay');
var conf = require('../config/wechat');
var http = require('../util/http');
var CryptoJS = require('crypto');
var xml2js = require('xml2js');
var md5 = require('MD5');

exports.buildXML = function(json){
    var builder = new xml2js.Builder();
    return builder.buildObject(json);
};

exports.parseXML = function(xml, fn){
    var parser = new xml2js.Parser({ trim:true, explicitArray:false, explicitRoot:false });
    parser.parseString(xml, fn||function(err, result){});
};

/**
 * 获取随机字符串
 */
exports.getNoncestr = function(){
    return q().then(function(){
        var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
        var result = "";
        for (var i = 0; i < 32; i++) {
            result += chars.charAt(Math.ceil(Math.random()*(chars.length-1)));
        }
        return result;
    })
};
/**
 * 对所有传入参数按照字段名的 ASCII 码从小到大排序（字典序）
 * @param params
 */
exports.sortByKey = function(params){
    return q().then(function(){
        var string = '';
        //取键key 排序
        var keyArr = new Array();
        var i = 0;
        _.mapValues(params, function(v, k){
            keyArr[i] = k+'='+v;
            i = keyArr.length;
        });
        keyArr = _.sortBy(keyArr);
        _.forEach(keyArr,function(eml){
            if(!string || string == '')
                string = eml;
            else
                string = string + '&' + eml;
        })
        return string;
    })
}
/**
 * 支付接口参数sign签名获取
 * @param params
 * @returns {*}-----{pay_sign:signature}
 */
exports.getPaySign = function(params){
    return q().then(function() {
        return exports.sortByKey(params);
    }).then(function(r){
        var string1 = r+'&key='+conf.key;
        logger.info('hash string', string1);
        //md5加密
        var signature = md5(string1);
        logger.info('md5 string', signature);
        //所有字符转为大写
        signature = signature.toUpperCase();
        logger.info('sign = '+signature);
        return signature;
    }).catch(function(e){
        logger.error(e.stack);
        return {err: e.message};
    });
};
/**
 * 支付接口参数package获取
 * @param params
 * @returns {*}-----{package: 'prepay_id='+r.prepay_id}
 */
exports.getPackage = function(params){
    var url = "https://api.mch.weixin.qq.com/pay/unifiedorder";
    var newParams = JSON.parse(params);
    return q().then(function() {
        return exports.getNoncestr();
    }).then(function(non){
        newParams.nonce_str = non;
        //获取签名
        return exports.getPaySign(newParams);
    }).then(function(sign) {
        newParams.sign = sign;
        return exports.buildXML(newParams);
    }).then(function(p){
        return http.postXml(url, p);
    }).then(function(r){
        return q.nfcall(exports.parseXML, r);
    }).then(function(result){
        //logger.info('prepay_id...',result);
        if (result.return_code=='SUCCESS' && result.result_code=='SUCCESS'){
            return {package: 'prepay_id='+result.prepay_id};
        }else{
            return {err:result.return_msg};
        }
    }).catch(function(e){
        logger.error(e.stack);
        return {err: e.message};
    });
};
/**
 * 获取支付最终参数---{appid:'',noncestr:'',package:'',signtype:'MD5',timestamp:'',signature:''}
 * @param params
 * @returns {*}-----{pay_params: data}
 */
exports.getPayParams = function(params){
    var data = {appId:conf.corp_id,nonceStr:'',package:'',signType:'MD5',timeStamp:''}
    return q().then(function(){
        return exports.getNoncestr();
    }).then(function(non){
        data.nonceStr = non;
        return exports.getPackage(params);
    }).then(function(pa) {
        data.package = pa.package;
        var date = new Date();
        var timestamp = date.getTime() + ""; // 必填，生成签名的时间戳
        timestamp = timestamp.substring(0, 10);
        data.timeStamp = timestamp;
        return exports.getPaySign(data);
    }).then(function(pay_sign){
        data.paySign = pay_sign;
        return {pay_params: data};
    }).catch(function(e){
        logger.error(e.stack);
        return {err: e.message};
    });
};
