'use strict';
var q = require('q'), _ = require('lodash');
var logger = require('log4js').getLogger('wechat');
var conf = require('../config/wechat');
var CryptoJS = require('crypto');
var fs = require('fs');
var path = require('path');
var http = require('../util/http');

var access = {
    token: '',
    ticket:'',
    expires: 0,
    ticket_time:0,
    time: 0
}

//基础access_token获取方法
exports.getToken = function(){
    var url ="https://api.weixin.qq.com/cgi-bin/token"
    return q().then(function(){
        if (!access.token || ((new Date().getTime() - access.time)*1000 + 1000 > access.expires)){
            return q().then(function(){
                return http.get(url, {appid: conf.corp_id, secret: conf.secret, grant_type:conf.grant_type});
            }).then(function(r){
                //r = JSON.stringify(r);
                logger.info(r);
                if (r.errmsg){
                    throw new Error(r.errmsg);
                }else{
                    access.token = r.access_token;
                    access.expires = r.expires_in;
                    access.time = new Date().getTime();
                    return access.token;
                }
            })
        }else{
            return access.token;
        }
    })
};

//网页授权access_token获取方法，从而获取openid
exports.getOpenid = function(code){
    var url = "https://api.weixin.qq.com/sns/oauth2/access_token";
    return q().then(function(){
        return q().then(function(){
            return http.get(url, {appid: conf.corp_id, secret: conf.secret, code:code, grant_type:conf.oauth_grant_type});
        }).then(function(r){
            //r = JSON.stringify(r);
            logger.info(r);
            if (r.errmsg){
                throw new Error(r.errmsg);
            }else{
                return r;
            }
        })
    })
};

exports.getUserInfo = function(code){
    var url = "https://qyapi.weixin.qq.com/cgi-bin/user/getuserinfo";
    var url_convert = "https://qyapi.weixin.qq.com/cgi-bin/user/convert_to_openid";
    var token;
    return q().then(function(){
        return exports.getToken(code);
    }).then(function(r){
        token = r;
        return http.get(url, {access_token: token, code: code});
    }).then(function(r){
        logger.info(r);
        if (r.errcode){
            throw new Error(r.errmsg);
        }else{
            if (r.UserId){
                //return {open_id: r.UserId, device_id: r.DeviceId};
                return q().then(function(){
                     return http.post1(url_convert, {access_token: token}, { userid: r.UserId});
                }).then(function(result){
                    logger.info(result);
                    if (result.errcode){
                        throw new Error(result.errmsg);
                    }else{
                        return {open_id: result.openid};
                    }
                })
            }else{
                return {open_id: r.OpenId, device_id: r.DeviceId};
            }
        }
    }).catch(function (e) {
        logger.warn(e.stack);
    })
};

exports.send = function(app, users, title, description, url){
    logger.info('wechat send', app, users, title, description, url);
    var params = {
       "touser": users.join('|'),
       "toparty": "",
       "totag": "",
       "msgtype": "news",
       "agentid": app,
       "news": {
           "articles":[
               {
                   "title": title,
                   "description": description,
                   "url": url
               }
           ]
       }
    }
    return exports.getToken().then(function(token){
        var url = "https://qyapi.weixin.qq.com/cgi-bin/message/send?access_token=" + token;
        return http.post(url, params)
    }).then(function(r){
        logger.info(r);
        if (r.errcode){
            throw new Error(r.errmsg);
        }else{
            return true;
        }
    })
};

exports.cardsend = function(openid,code,policy_sn,formatStr,price,life) {
    var token;
    var params = {
        "card": {
            "card_type": "GENERAL_COUPON",
            "general_coupon": {
                "base_info": {
                    "logo_url":
                        "http://mmbiz.qpic.cn/mmbiz/iaL1LJM1mF9aRKPZJkmG8xXhiaHqkKSVMMWeN3hLut7X7hicFNjakmxibMLGWpXrEXB33367o7zHN0CwngnQY7zb7g/0",
                    "brand_name":"富邦延保",
                    "code_type":"CODE_TYPE_BARCODE",
                    "title": "延保支付成功",//"延保支付成功",
                    "sub_title": "美心保",//"美心宝",
                    "color": "Color080",
                    "notice": "延保注册码："+policy_sn,//"作为延保的依据",
                    "service_phone": "400-6941-123",
                    "description": "可向延保上架提供此卡卷",//"请在投保商品需要延保时提供",
                    "date_info": {
                        "type": 2,
                        "fixed_term": life*365,
                        "fixed_begin_term": 0
                    },
                    "sku": {
                        "quantity": 500000
                    },
                    "get_limit": 1,
                    "use_custom_code": false,
                    "bind_openid": false,//此处需要修改
                    "can_share": false,
                    "can_give_friend": false
                },
                "default_detail": "尊敬的客户，您的美心保延保已注册支付成功，合同于"+formatStr+"生效，延保注册码"+policy_sn+"，延保费用为"+price+"元，延保期为商品厂保期结束后的"+life+"年。美心保报修和后续服务商是深圳富邦恒安商业服务有限公司，延保服务热线400-6941-123。谢谢！"//"延保支付成功"
            }
        }
    }

    return exports.getToken().then(function(r){
        token = r;
        logger.info(token);
        var url = "https://api.weixin.qq.com/card/create";
        return http.post1(url, {access_token: token}, params);
    }).then(function(r){
        logger.info(r.card_id);
        if (r.errcode){
            throw new Error(r.errmsg);
        }else{
            logger.info('code.........'+code);
            var nonce_str = "abcdefghijklmnopqrstuvwxyz0123456789"

            var date = new Date();
            var timestamp = date.getTime() + ""; // 必填，生成签名的时间戳
            timestamp = timestamp.substring(0, 10);

            var shasum = CryptoJS.createHash('sha1');
            shasum.update(conf.secret+timestamp+r.card_id+code+nonce_str);
            var signature = shasum.digest('hex');
            return http.post1("https://api.weixin.qq.com/cgi-bin/message/custom/send", {access_token: token},
                {
                    "touser":openid,
                    "msgtype":"wxcard",
                    "wxcard":{
                        "card_id":r.card_id,
                        "card_ext": {"timestamp":timestamp,"signature":signature}
                    }
                }).then(function(result){
                    //logger.info(result);
                    if (result.errcode){
                        throw new Error(result.errmsg);
                    }else{
                        return result;
                    }
                }
            )
        }
    }).catch(function (e) {
        logger.warn(e.stack);
    })
};

exports.getJsapiTicket = function(){
    var url = "https://api.weixin.qq.com/cgi-bin/ticket/getticket";

    return q().then(function(){
        if (!access.ticket || ((new Date().getTime() - access.ticket_time)*1000 + 1000 > access.expires)){
            return q().then(function(){
                return exports.getToken();
            }).then(function(token){
                logger.info('param...',{access_token:token, type:conf.type});
                return http.get(url,{access_token:token, type:conf.type});
            }).then(function(r){
                logger.info('getJsapiTicket...',r);
                if(r.errcode != 0){
                    throw new Error(r.errmsg);
                    alert('getJsapiTicket error:'+JSON.stringify(res));
                }else{
                    access.ticket = r.ticket;
                    access.expires = r.expires_in;
                    access.ticket_time = new Date().getTime();
                    return {ticket: r.ticket};
                }
            })
        }else{
            return {ticket: access.ticket};
        }
    })
};

exports.downloadPic = function(code , serverId,fileName){
    var url = "http://file.api.weixin.qq.com/cgi-bin/media/get";
    var token;
    return q().then(function(){
        return exports.getToken(code);
    }).then(function(r){
        token = r;
        //logger.info('access_token:',token,';media_id:',serverId);
        return http.getImg(url, {access_token: token, media_id: serverId},path.join(__dirname,"../../uploads/wechat", fileName));
    }).then(function () {

    }).catch(function (e) {
        logger.warn(e.stack);
    })
};