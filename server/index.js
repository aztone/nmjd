var DBL = require('./dbl/dbl');
var q = require('q');
var confdb = require('./config/db');
var wechat = require('./wechat/wechat');
var logger = require('log4js').getLogger('router');

module.exports = function (app) {
    app.use('/app/', require('./controllers/app'));
    app.use('/access', require('./controllers/access'));
    app.all('/api/*', function (req, res, next) {
        /*if (!req.session.user){
            var e = new Error('未授权请重新登录');
            e.status = 401;
            throw e;
        }else{
            req.dbl =  new DBL(req.session.dbid);
            next();
        }*/

        req.dbl = new DBL();
        next();
    });

    //微信
    app.all('/nmjd_pay/*', function (req, res, next) {
        var code = req.param('code');
        req.dbl = new DBL();

        var buffer = [];
         req.on('data', function(trunk){
         buffer.push(trunk);
         });
         req.on('end', function(){
         req.rawbody = Buffer.concat(buffer).toString('utf8');
         next();
         });
         req.on('error', function(err){
         next(err);
         });
    });
    //微信
    app.all('/nmjd/*', function (req, res, next) {
        var code = req.param('code');
        req.dbl = new DBL();
        next();
    });



    app.use('/nmjd/wechat', require('./controllers/wechat'));
    app.use('/nmjd/wechat_pay', require('./controllers/wechat_pay'));

    app.use('/nmjd/sheet', require('./controllers/sheet'));
    app.use('/nmjd/card', require('./controllers/card'));
    app.use('/nmjd/upload', require('./controllers/upload'));
    app.use('/nmjd_pay/notify', require('./controllers/sheet'));

    app.use('/nmjd_main', function(req, res){
        var code = req.query.code;
        res.redirect('/#/nmjd/jdqj/main?code='+code);
    });

};