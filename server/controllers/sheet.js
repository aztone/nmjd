/**
 * Created by aztone on 2015/4/6.
 */

'use strict';

var express = require('express');
var router = express.Router();
module.exports = router;
var iconv = require('iconv-lite');
iconv.extendNodeEncodings();

var q = require('q'), _ = require('lodash'), logger = require('log4js').getLogger('sheet'), fs = require('fs'), path = require('path');
var wechat = require('../wechat/wechat');
var wechat_pay = require("../wechat/wechat_pay");

router.get('/', function (req, res) {
    var where = req.param('where');
    var page = req.param('page');
    var limit = req.param('limit');
    logger.info(where);
    q().then(function () {
        return req.dbl.QxjCardFind(where, page, limit);
    }).then(function (data) {
        res.json({result: data});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.get('/sheet', function (req, res) {
    var where = req.param('where');
    var page = req.param('page');
    var limit = req.param('limit');
    logger.info(where);
    q().then(function () {
        return req.dbl.cleanSheetFind(where, page, limit);
    }).then(function (data) {
        res.json({result: data});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.get('/:code', function (req, res) {
    var code = req.param('code');
    q().then(function () {
        return req.dbl.QxjCardFindByCode(code);
    }).then(function (data) {
        res.json({result: data});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.post('/register', function (req, res) {
    var qxj = req.param('qxj');
    logger.info(qxj);
    q().then(function () {
        return req.dbl.cleanSheetAdd(qxj);
    }).then(function (data) {
        res.json({result: data});
    }).catch(function (e) {
        logger.warn(e.stack);
        res.json({err: e.message});
    })
})


router.post('/import_card/:file', function (req, res) {
    var file = req.param('file');
    var count = {total:0, success: 0, fail: 0, nobc:0, fielderror: 0, formaterror: 0};

    q.Promise(function (resolve, reject) {
        var filePath = path.join(__dirname, '../../uploads', 'import', file);
        var first = true;
        var content = fs.readFileSync(filePath, {encoding: 'gbk'});

        var lines = content.split('\n');
        var data = [];
        for (var i = 0; i < lines.length; i++) {
            var line = lines[i].trim();
            //console.log(line);
            if (i == 0) {
                if (line != '清洁卡号,清洁卡类型,抵扣金额') {
                    reject(new Error('文件格式不正确ȷ'));
                }
            } else {
                var a = line.split(',');
                if (a[0]) {
                    var code = (a[0] || "").replace(/(^\s*)|(\s*$)/g, "");
                    if (!_.find(data, {code: code})){
                        data.push({code: code, cardType: a[1], deductible_amount:a[2]});
                    }
                }
            }
        }
        if (data.length > 0) {
            resolve(data);
        } else {
            reject(new Error('文件无数据'));
        }
    }).then(function (data) {
        var p = q();
        data.forEach(function (card) {
            // return req.dbl.axbCardImport(card);
            p = p.then(function () {
                return req.dbl.QxjCardImport(card).then(function (err) {
                    count.total = count.total+1;
                    if (err == 0){
                        count.success = count.success+1;
                    }else{
                        count.fail = count.fail+1;
                        logger.info('导入异常数据:', card, err);
                        if (err == 1){
                            count.nobc = count.nobc+1;
                        }else if(err == 2){
                            count.fielderror = count.fielderror+1;
                        }
                    }
                });
            })
        });
        return p;
    }).then(function () {
        res.json({result: count})
    }).catch(function (e) {
        console.log(e.stack)
        res.json({err: e.message});
    })
});



router.all("/notify_url_qxj",function(req,res){
    var data = {};
    q().then(function(){
        return q.nfcall(wechat_pay.parseXML, req.rawbody);
    }).then(function(result){
        logger.info('notify_url...',result);
        if (result.return_code=='SUCCESS' && result.result_code=='SUCCESS'){
            //修改本地订单
            data.out_trade_no = result.out_trade_no;//商户订单号
            data.bank_type = result.bank_type;//付款银行类型
            data.transaction_id = result.transaction_id;//订单号
            data.time_end = result.time_end;//支付完成时间
            data.state = 'Y';

            logger.info('notify_url...data',data);
            return q().then(function () {
                return req.dbl.cleanSheetModify(result.out_trade_no, data);
            }).catch(function (e) {
                logger.warn(e.stack);
                res.json({err: e.message});
            });
        }else{
            res.json(wechat_pay.buildXML({return_code:'FAIL'}));
        }
    }).then(function(r){
        res.json(wechat_pay.buildXML({return_code:'SUCCESS'}));
    }).catch(function(err){
        logger.info(err.stack);
        res.json(wechat_pay.buildXML({return_code:'FAIL',return_msg: err.message}));
    })
});

router.put('/:_id', function (req, res) {
    var card = req.param('card');
    var _id = req.param('_id');
    q().then(function () {
        return req.dbl.CardModify(_id, card);
    }).then(function () {
        res.json({result: true});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});
