/**
 * Created by wing on 2015/10/13.
 */
var express = require('express');
var router = express.Router();
var wechat_pay = require("../wechat/wechat_pay");
var wechat = require("../wechat/wechat");
var CryptoJS = require('crypto');
var logger = require('log4js').getLogger('wechat_pay');
module.exports = router;
var q = require('q'), _ = require('lodash');
var conf = require('../config/wechat');
var http = require('../util/http');

router.get('/appid', function(req, res){
    res.json({result: conf.corp_id});
});
router.get('/mch_id', function(req, res){
    res.json({result: conf.mch_id});
});
router.get('/spbill_create_ip', function(req, res){
    res.json({result: conf.spbill_create_ip});
});

router.get('/url', function(req, res){
    res.json({result: conf.url});
});

router.get('/openid', function(req, res){
    var code  = req.param('code');
    q().then(function(){
        return wechat.getOpenid(code);
    }).then(function (r) {
        res.json({result: r.openid});
    }).catch(function (err) {
        console.log(err.stack);
        res.json({err: err.message});
    })
});

router.get("/pay_params",function(req,res){
    var params = req.param("params");
    q().then(function(){
        logger.info('get pay_params params....');
        logger.info(params);
        return wechat_pay.getPayParams(params);
    }).then(function(r){
        logger.info('getPayParams...result...',r);
        res.json({result: r.pay_params});
    }).catch(function(err){
        logger.info(err.stack);
        res.json({err: err.message});
    })
});

router.all("/notify_url",function(req,res){
    //logger.info('notify_url...',req.rawbody);
    q().then(function(){
        return q.nfcall(wechat_pay.parseXML, req.rawbody);
    }).then(function(result){
        logger.info('notify_url...',result);
        if (result.return_code=='SUCCESS' && result.result_code=='SUCCESS'){
            //修改本地订单

        }else{
            res.json(wechat_pay.buildXML({return_code:'FAIL'}));
        }
    }).then(function(r){
        res.json(wechat_pay.buildXML({return_code:'SUCCESS'}));
    }).catch(function(err){
        logger.info(err.stack);
        res.json(wechat_pay.buildXML({return_code:'FAIL',return_msg: err.message}));
    })
});