/**
 * Created by wing on 2015/3/17.
 */
var express = require('express');
var router = express.Router();
module.exports = router;

var q = require('q'), _ = require('lodash');
var DBL = require('../dbl/dbl');

router.post('/login', function(req, res){
    var name = req.param('name');
    var passwd = req.param('passwd');
    var dbid = require('../config/db').dbid;

    var dbl;
    q().then(function () {
        if (!dbid || !name || !passwd){
            throw new Error('信息不完整');
        }
        dbl = new DBL(dbid);
        return dbl.userFind({name: name});
    }).then(function (users) {
        if (users.length <= 0){
            throw new Error('用户不存在');
        }else if (users[0].password != passwd){
            throw new Error('密码不正确');
        }else{
            req.session.user = users[0];
            req.session.dbid = dbid;
        }
    }).then(function () {
        res.json({result: req.session.user});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.post('/logout', function(req, res){
    req.session.user = null;
    req.session.dbid = null;
    res.json({result: true});
});

router.get('/info', function(req, res){
    res.json({result: req.session.user});
});