/**
 * Created by wing on 2015/4/18.
 */
'use strict';
var express = require('express');
var router = express.Router();
module.exports = router;

var q = require('q'), _ = require('lodash'), fs= require('fs'), path = require('path'), logger = require('log4js').getLogger('upload');

router.get('/:type/:file', function (req, res) {
    var type = req.param('type');
    var file = req.param('file');
    var filePath = path.join(__dirname, '../../uploads', type, file);
    logger.info('filePath:',filePath);
    if (fs.existsSync(filePath)){
        res.sendFile(filePath);
    }else{
        res.status(404).send('文件不存在');
    }
});

router.post('/:type', function (req, res) {
    var type = req.param('type');
    if (!type){
        res.status(404).send('路径不存在');
    }else{
        q().then(function () {
            if (!fs.existsSync(path.join(__dirname, '../../uploads', type))){
                fs.mkdirSync(path.join(__dirname,'../../uploads', type));
            }

            var file = req.files.file.name;
            var oldPath = path.join(__dirname, '../../uploads', file);
            var newPath = path.join(__dirname, '../../uploads', type, file);
            fs.renameSync(oldPath, newPath);
            res.send(JSON.stringify({file: file}));
        }).catch(function (e) {
            logger.warn(e.stack);
            res.status(500).send(e.message);
        })
    }

});