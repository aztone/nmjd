/**
 * Created by wing on 2015/4/27.
 */
'use strict';
var express = require('express');
var router = express.Router();
module.exports = router;

var q = require('q'), _ = require('lodash'), logger = require('log4js').getLogger('metadata');

router.get('/all', function (req, res) {
    q().then(function () {
        return req.dbl.metadataListFind();
    }).then(function (docs) {
        res.json({result: docs});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.put('/sms', function (req, res) {
    q().then(function () {
        var sms = req.param('sms')
        return req.dbl.smsModify(sms);
    }).then(function (docs) {
        res.json({result: docs});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.get('/key', function (req, res) {
    q().then(function () {
        return req.dbl.objectId();
    }).then(function (objectId) {
        res.json({result: objectId})
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.get('/ref', function (req, res) {
    var context = req.param('where');
    q().then(function () {
        return req.dbl.metadataRefFind(context);
    }).then(function (docs) {
        res.json({result: docs});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.get('/:context', function (req, res) {
    var context = req.param('context');
    q().then(function () {
        return req.dbl.metadataFind(context);
    }).then(function (docs) {
        res.json({result: docs});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});


router.get('/', function (req, res) {
    q().then(function () {
        return req.dbl.metadataFind();
    }).then(function (docs) {
        res.json({result: docs});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.post('/', function (req, res) {
    var metadata = req.param('metadata');
    q().then(function () {
        return req.dbl.metadataAdd(metadata);
    }).then(function () {
        res.json({result: true});
    }).catch(function (e) {
        logger.warn(e.stack);
        res.json({err: e.message});
    })
});

router.put('/:_id', function (req, res) {
    var _id = req.param('_id');
    var metadata = req.param('metadata');
    q().then(function () {
        return req.dbl.metadataModify(_id, metadata);
    }).then(function () {
        res.json({result: true});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.delete('/:_id', function (req, res) {
    var _id = req.param('_id');
    var metadata = req.param('metadata');
    q().then(function () {
        return req.dbl.metadataModify(_id, {state: 'N'});
    }).then(function () {
        res.json({result: true});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});
