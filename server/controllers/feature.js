/**
 * Created by wing on 2015/4/29.
 */
'use strict';
var express = require('express');
var router = express.Router();
module.exports = router;

var q = require('q'), _ = require('lodash'), logger = require('log4js').getLogger('feature');

router.get('/', function (req, res) {
    q().then(function () {
        return req.dbl.featureFind();
    }).then(function (docs) {
        res.json({result: docs});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});