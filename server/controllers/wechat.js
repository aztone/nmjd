/**
 * Created by sarry on 2015/6/03.
 * 微信
 */
var express = require('express');
var router = express.Router();
var wechat = require("../wechat/wechat");
var CryptoJS = require('crypto');
var logger = require('log4js').getLogger('wechat');
module.exports = router;

var q = require('q'), _ = require('lodash');
//var DBL = require('../dbl/dbl');

var conf = require('../config/wechat');
var http = require('../util/http');


router.get('/gourl', function(req, res){
    res.json({result: conf.go_url});
});

router.get('/openid', function(req, res){
    var data  = req.param('data');
    q().then(function () {
        //console.log('user open_id：'+req.session.user.open_id);
        res.json({result: req.session.user});
    }).catch(function (err) {
        console.log(err.stack);
        res.json({err: err.message});
    })
});

router.get('/appid', function(req, res){
    res.json({result: conf.corp_id});
    logger.info('appid', {result: conf.corp_id});
});

router.get('/url', function(req, res){
    res.json({result: conf.url});
    logger.info('url', {result: conf.url});
});

router.get('/nonceStr', function(req, res){
    var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
    var result = "";
    for (var i = 0; i < 32; i++) {
        result += chars.charAt(Math.ceil(Math.random()*(chars.length-1)));
    }
    res.json({result: result});
    logger.info('nonceStr', {result: result});
});

router.get('/jsapi_ticket', function(req, res){
    logger.info('start jsapi_ticket...');
    q().then(function(){
        return wechat.getJsapiTicket();
    }).then(function(result){
        res.json({result: result.ticket});
    }).catch(function(e){
        logger.error(e.stack);
        res.json({err: e.message});
    })
});

router.get('/signature', function(req, res){
    var param = req.param("param");
    q().then(function(){
        param = JSON.parse(param);
        logger.info('param = '+param);
        var string1 = undefined;
        _.mapValues(param, function(v, k){
            if(string1)
                string1 = string1+'&'+k+'='+v;
            else
                string1 = k+'='+v;
        });
        logger.info('hash string', string1);
        var shasum = CryptoJS.createHash('sha1');
        shasum.update(string1);
        var signature = shasum.digest('hex');
        logger.info('signature = '+signature);
        return signature;
    }).then(function(result){
        res.json({result: result});
    }).catch(function(e){
        logger.error(e.stack);
        res.json({err: e.message});
    });
});

router.get('/location',function(req,res){
    //var url = 'http://maps.google.cn/maps/api/geocode/json?latlng=39.90923,116.397428&sensor=true';
    var url = 'http://maps.google.cn/maps/api/geocode/json';
    var latlng = req.param('lat')+','+req.param('lng');
    logger.info('in server location...');
    q().then(function() {
        return http.get(url, {latlng: latlng, sensor: true});
    }).then(function(r){
        var address = {address:''};
        if(r.status=='OK')
            address = {address:r.results[0].formatted_address};
        logger.info('address '+address);
        res.json({result:address});
    }).catch(function(e){
        logger.error(e.stack);
        res.json({err:e.message});
    });

});

router.get('/downloadImg', function(req, res){
    var code = req.param('code');
    var serverId = req.param('serverId');
    var fileName = req.param('fileName');
    q().then(function(){
        return wechat.downloadPic(code, serverId,fileName);
    }).then(function(){
        res.json({result: true});
    }).catch(function(e){
        logger.error(e.stack);
        res.json({err: e.message});
    })
});


