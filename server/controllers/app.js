/**
 * Created by wing on 2015/4/29.
 */
var express = require('express');
var router = express.Router();
module.exports = router;

var q = require('q'), _ = require('lodash');

router.get('/', function (req, res) {
    res.json({result: require('../config/app')});
});