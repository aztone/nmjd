/**
 * Created by wing on 2015/4/27.
 */
'use strict';
var express = require('express');
var router = express.Router();
module.exports = router;

var q = require('q'), _ = require('lodash'), logger = require('log4js').getLogger('user');

router.get('/', function (req, res) {
    var where = req.param('where');
    q().then(function () {
        return req.dbl.userFind(where);
    }).then(function (docs) {
        res.json({result: docs});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.get('/:_id', function (req, res) {
    var _id = req.param('_id');
    q().then(function () {
        return req.dbl.userFindOne({_id:_id});
    }).then(function (doc) {
        res.json({result: doc});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.post('/:_terminalId', function (req, res) {
    var _terminalId = req.param('_terminalId');
    var user = req.param('user');
    q().then(function () {
        return req.dbl.userAdd(_terminalId, user);
    }).then(function () {
        res.json({result: true});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});

router.put('/:_id', function (req, res) {
    var _id = req.param('_id');
    var user = req.param('user');
    q().then(function () {
        return req.dbl.userModify(_id, user);
    }).then(function () {
        res.json({result: true});
    }).catch(function (e) {
        res.json({err: e.message});
    })
});