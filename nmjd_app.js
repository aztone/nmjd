
var path = require('path'), fs = require('fs');
if (!fs.existsSync(path.join(__dirname, 'logs'))){
    fs.mkdirSync(path.join(__dirname, 'logs'))
}
var log4js = require('log4js');
log4js.configure('./server/config/log4js.json');
var logger = log4js.getLogger('app');

var express = require('express'), favicon = require('serve-favicon'), session = require('cookie-session');
var cookieParser = require('cookie-parser'), bodyParser = require('body-parser'), multer = require('multer');

process.env.PORT = require('./server/config/app').port;
var app = express();
app.use(log4js.connectLogger(log4js.getLogger("http"), { level: 'auto', nolog: [
    '\\.css', '\/js\/', '\\.html', '\/img\/', '\/fonts\/', '\/bower_components\/'
    ]  }));
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));
app.use(cookieParser());
app.use(session({keys: ['wxt-policy'], cookie: {maxAge: 60*60*10000}}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(multer({ dest: './uploads/' }));

require('./server')(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    res.send(JSON.stringify({
        message: err.message
    }));
    logger.warn(err.message);
});

// error handlers
app.use(function(err, req, res) {
    res.status(err.status || 500);
    res.send(JSON.stringify({
        message: err.message,
        error: err
    }));
    logger.warn(err.stack);
});

app.set('port', process.env.PORT);
var server = app.listen(app.get('port'), function() {
    logger.info('Express server listening on port ' + server.address().port);
});


